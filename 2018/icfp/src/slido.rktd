;; (struct slido-question (timestamp text author) #:prefab)
(#s(slido-question
    "2018-09-26T14:10:00"
    "Since session types enforce a protocol so strictly, is the \"R\" cost just half as much as the \"RS\" cost?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:13:00"
    "Does your system support cost models with randomness?"
    "Michael Klein")
 #s(slido-question
    "2018-09-26T14:15:00"
    "Is that a cartoon of Jackie Chan?"
    "Rudy Depena")
 #s(slido-question
    "2018-09-26T14:20:00"
    "Can you prove upper bounds on time delays without necessarily knowing the _exact_ delay?"
    "davidad")
 #s(slido-question
    "2018-09-26T14:21:00"
    "Is this box type essentially a buffer?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:21:00"
    "Your modalities are similar to linear logic exponential bang which also require global sequent rule. Can you comment on that?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:22:00"
    "Have you thought about deriving space use using similar methods?"
    "John Hughes")
 #s(slido-question
    "2018-09-26T14:23:00"
    "What proof method did you use for proving soundness? Semantic models?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:23:00"
    "Could your system integrate infinite streams?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-26T14:24:00"
    "Is that a cartoon of me? :)"
    "Jackie Chan")
 #s(slido-question
    "2018-09-26T14:25:00"
    "Are your temporal specifications enforced at run time?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:26:00"
    "I guess a sender and receiver must agree on exactly when they will communicate, or deadlock?"
    "John Hughes")
 #s(slido-question
    "2018-09-26T14:43:00"
    "Have you considered making a cost semantics that you prove sound with your semantics?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:45:00"
    "Performance in practice involves low level details like memory hierarchies. Does your machine closure technique apply to these kinds of machine models?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:45:00"
    "How difficult is it to consider improvement in the presence of laziness?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:47:00"
    "How accurate is an operational notion of cost compared to the compiled  implementation?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:54:00"
    "What is the type of self application: \"lambda x. x x\"?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T14:57:00"
    "Can forall quantifier in System F be understood as infinite intersection type? Is that understanding precise?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:01:00"
    "Is a type system really static if it is undecidable?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:01:00"
    "`lambda x . x x` has the type `{A -> A, A} -> A`, I believe."
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:04:00"
    "(not a question) (non-)idempotent type systems are used as intermediate proof steps or to define denotational semantics, not as static checkers for surface programs. We don't need to worry about decidability much."
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:06:00"
    "Is there a programming language that implements intersection types?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:07:00"
    "Does the quantitative system contain a bang (!) type former? Does the decidability result still hold?"
    "James")
 #s(slido-question
    "2018-09-26T15:09:00"
    "Is the intersection type size proportional to the length of evaluation making types very big? Are they unusable with function arguments that might affect evaluation length?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:11:00"
    "Can intersection types be added to more expressive systems like System F or CoC?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:11:00"
    "From a type derivation for a term, can you find a minimal derivation for the term? (Is this equivalent to normalizing it?)"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:12:00"
    "Can you separate the substitution-performing steps that are necessary for computation from those that lead to the size explosion problem?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:12:00"
    "Is it possible to define relation to linear logic precisely?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:15:00"
    "How can we understand these derivations as staatic, since they effectively encode the reduction paths?"
    "Aaron Stump")
 #s(slido-question
    "2018-09-26T15:16:00"
    "Aaron: they give an inductive structure to the space of reduction paths."
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:34:00"
    "Do we have similar pitfalls in trusting the pretty printer as we have in trusting an elaborator in a proof assistant?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:37:00"
    "Not a question: maybe \"wishful thinking\" could convey your goal without the controversial suggestion that \"faith\" means lack of evidence."
    "Aaron Stump")
 #s(slido-question
    "2018-09-26T15:38:00"
    "Shouldn't you have two \"suc\"s in the last clause result?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:40:00"
    "Will Agda gain support for inspecting case trees directly?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:40:00"
    "Can your elaboration process deal with clauses which have a different number of arguments on the LHS?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:46:00"
    "Does this algorithm work for a system without copatterns by ignoring the copattern parts?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:46:00"
    "You give an inductive characterization of elaboration; do you provide a decision procedure for it?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:47:00"
    "can you do the timer example with a definition of Stream with the constraint head > 0 instead of there exists m, head = S m ?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:48:00"
    "In Agda, refl is defined rather than being builtin. Why is tell appreciable in your language?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-26T15:48:00"
    "What would it take to adapt this process to case expressions?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:49:00"
    "Is that actually the Agda logo?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T15:58:00"
    "What is the type of l2v? Don't you need an existential?"
    "Richard Eisenberg")
 #s(slido-question
    "2018-09-26T16:00:00"
    "Why impredicativity? It's inconsistent with all kinds of axioms (choice, classical, ...)."
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:02:00"
    "How does Cedille compare to the calculus of implicit constructions?"
    "Jesper")
 #s(slido-question
    "2018-09-26T16:03:00"
    "How is the erased function space in Cedille related to Agda's irrelevant arguments? What about Nuprl's dependent family intersections?"
    "David Christiansen")
 #s(slido-question
    "2018-09-26T16:04:00"
    "Does this work poses new challenges due to dependent language compared to the ”Safe Zero-Cost Coercions for Haskell” which does very similar thing to the Haskell's newtype-mechanism. Could you reuse some of the ideas from there?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-26T16:04:00"
    "How does that compare to the theory of ornements (McBride, Gibbons) which can also be used to translate code operating on lists to code operating on vectors? (In agda)Is that the reverse transformation?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:05:00"
    "Does this mean all indices will gone at runtime (and so vector length will be linear)?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:09:00"
    "With pervasive heterogeneous equality, isn't there a risk that two terms which are semantically different might happen to have the same underlying representation?"
    "Dan Burton")
 #s(slido-question
    "2018-09-26T16:10:00"
    "Does Cedille have decidable typechecking?"
    "Jesper")
 #s(slido-question
    "2018-09-26T16:11:00"
    "Is type-checking in Cedille decidable ?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:11:00"
    "The zero-cost aspect comes naturally by placing yourself in Cedille’s extrinsic setting. What is the key lesson here that could be applied to intrinsic theories?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:12:00"
    "What happens in the other direction, when you can't reconstruct the entire information?"
    "Beta")
 #s(slido-question
    "2018-09-26T16:12:00"
    "(I would be interested in hearing more about the comparison to ornaments.)"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:13:00"
    "Can we play the same sort of games with codata?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:14:00"
    "In AppV to AppL by combinators example, does the (somewhat unusual) place of m parameter play signficant role?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-26T16:16:00"
    "Is Cedille normalizing? Does it type all normalizing terms?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T16:16:00"
    "W.rt. Church encoding, how do you get induction?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:16:00"
    "Does including runtime type validation in a language that uses Erasure change any of it's properties or how it is classified?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:17:00"
    "Can you elaborate on the other two approaches you identified, and your intuition as to where they lie in the hierarchy and why?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-26T12:17:00"
    "In what way do the types in ->E systems help programmers?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:17:00"
    "Another approach is \"concrete\" typing, from your colleague Jan Vitek and others, claimed to be quite fast. Can it fit into your framework?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-26T12:18:00"
    "Can the three instances of soundness be recast as three different semantics of the same syntax for types?"
    "Ron Garcia")
 #s(slido-question
    "2018-09-26T12:20:00"
    "We know that the higher-order semantics can, in theory, impactthe time complexity of a program.  Have you experimented withchanging the input sizes for your benchmarks to discover whetherthere are impacts on time complexity?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-26T12:20:00"
    "Do you think the linear overhead of the first-order approach can be removed with more efficient cast insertion?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:23:00"
    "How do you think the performance will look like when we have micro gradual typing?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:23:00"
    "Do you have any ideas for optimization for the first order semantics?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:23:00"
    "H, as described, includes runtime checks of return values on lambdas. Could H instead perform some sort of static analysis of the lambda from dynamic land at compile time, to raise the error at compile time as well as avoid the cost of the runtime check?"
    "Dan Burton")
 #s(slido-question
    "2018-09-26T12:25:00"
    "Hack is marked as \"erasure\" but actually it checks head constructors at function entry and exit."
    "Andrew Kennedy")
 #s(slido-question
    "2018-09-26T12:31:00"
    "On the lattice example at the beginning, the node at the top is slower than the one below-right. How can it ever happen that the top node is not the fastest?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:32:00"
    "How can a fully typed program be slower than a partially typed one?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:35:00"
    "The cost lattice suggest you're using the first-order semantics described in the prior talk; can your cost semantics scale to other semantic like higher-order?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:37:00"
    "How do you ensure that the variational technique doesn't blow up exponentially like the naive version?"
    "Joey Eremondi")
 #s(slido-question
    "2018-09-26T12:38:00"
    "I wonder if there is a correlation between the cast numbers as predicted by their technique, and the actual overhead when running the programs."
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:42:00"
    "In the beginning you mentioned static-to-dynamic direction. Can the tool suggest a configuration trajectory for this direction?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:43:00"
    "Do you need to provide all configurations ahead of time before your tool can pick the best migration path?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:43:00"
    "What language were your experiments conducted in?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-26T12:44:00"
    "What about type-directed optimizations? Can their effect be included in the cost model?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:44:00"
    "How does your cost analysis account for the number of times acast may be applied during an execution of the program?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-26T12:45:00"
    "This seems to assume the type annotations are available, either through coder effort or inference. Have you had realistic examples where using the available types wasn’t the fastest?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:45:00"
    "How often do people end up with pathological configurations when adding type annotations manually?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:45:00"
    "In your experiment, what was the balance of typed and untyped boundaries recommended by the program?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:45:00"
    "Your example had variables B C G corresponding to counts. Where do they cone from?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-26T12:46:00"
    "What about abandoning existing type annotations in the interests of performance?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:48:00"
    "Does your cost semantics account for space-efficient coercions or does it use the straightforward higher-order approach?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-26T12:57:00"
    "What's the ordering on the values in \"behave the same\"?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T12:57:00"
    "Did you try applying this technique to the models that tried & failed to prove graduality?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:03:00"
    "Is there non-anecdotal evidence to support the view that writing programs is quicker in untyped languages which appears to be a motivation for gradual typing? Does it include a cost-benefit analysis of in terms of both programmer time and program correctness."
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:04:00"
    "Your equivlance up to embedding and projection suggests you have an equational theory for optimizatimg away casts; have you thought about this?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:04:00"
    "Is \"embedding-projection pair\" related to an adjunction? Is it halfway between an adjunction and an equivalence?"
    "davidad")
 #s(slido-question
    "2018-09-26T13:06:00"
    "Could your contextual error approximation be augmented with costs, to also capture a notion of \"is faster\"?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-26T13:07:00"
    "So is your logical relation defined by induction on the proof that a type is less precise than another?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:08:00"
    "So does this help designing gradual parametric languages?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:08:00"
    "If EP pairs form an adjunction (see the other question), then is there reasonable reformulation in terms of monads?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-26T13:08:00"
    "How much smaller is the proof via logical relations versus simulation?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-26T13:08:00"
    "Following the keynotes analogy of analogy as abstraction, are both graduality and parametricity instances of some abstract object?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:19:00"
    "Seems you use ssreflect. What advantages does it provide for you?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:21:00"
    "Did you find invariants in comments that were NOT tested by QuickCheck properties?"
    "John Hughes")
 #s(slido-question
    "2018-09-26T13:22:00"
    "Why is list not capitalized on slide 11"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:22:00"
    "Would it be reasonable/possible to replace property tests in practice with formal verification in practice?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:22:00"
    "Can hs-to-coq be used to get more efficient libraries for Coq?"
    "Jesper")
 #s(slido-question
    "2018-09-26T13:22:00"
    "Did you try verifying an old, buggy version of the containers library?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:22:00"
    "So, what bugs did you find in the real-world Haskell?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-26T13:23:00"
    "Can you reason about laziness?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:25:00"
    "You get error == error, is that faithful"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:28:00"
    "Does your approach scales to verifing interesting properties of GHC ?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:29:00"
    "Did you have to extend or change the datatype invariants you found in comments for the proofs to succeed?"
    "Peter")
 #s(slido-question
    "2018-09-26T13:29:00"
    "Since you had to borrow some techniques from Isabelle, do you have any plans to build hs-to-isabelle next?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:30:00"
    "You bridged the gap between Haskell and Coq semantics with clever Coq tricks! (like with error). Did you cover all ? Would there be  programs you could prove correct with Coq but don’t behave as expected in Haskell ?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-26T13:30:00"
    "Can you explain how you define ptreq to enable proving ptrEq_eq?"
    "Aaron Stump")
 #s(slido-question
    "2018-09-26T13:31:00"
    "What do you assume about Haskell's and Coq's representation of integers - and how does that influence (or jeopardize) the trust we should put in the results?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:31:00"
    "did you verify any typeclass laws?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:31:00"
    "Did you encounter any limitations of CoQ?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:31:00"
    "Thick of making ptr equality opaque seems unsound. When you call it twice on the same values, you get the same value. I would expect it to be properly non-deterministic in haskell."
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:32:00"
    "Is the glass going to break?"
    "John clements")
 #s(slido-question
    "2018-09-26T13:32:00"
    "Were you able to make use of any auto tactics in coq to reduce the tediousness of the proof?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:32:00"
    "Is there a reusable intermediate representation that other languages could compile down to and go to readable coq?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T13:33:00"
    "Here’s an interesting property:Prop xs = forAll (elements xs) $ \\x -> x elem xsQuichCheck finds a counter example... can you prove it?"
    "John Hughes")
 #s(slido-question
    "2018-09-26T13:34:00"
    "are there any major blockers to merging the verification stuff into the containers library source and verifying during build?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:48:00"
    "Could you, please, go back to the power example? I didn't follow how do you generate actual code with lets out of CMon representation"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-26T09:48:00"
    "Could your representations also be applicable to doing proofs by reflection in something like Coq?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:48:00"
    "Did you consider rewriting by the algebraic properties rather than directly using the free structure which may be difficult to find or represent?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:49:00"
    "Is orienting of algebra laws equations always sufficient?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:49:00"
    "Is it feasible for the compiler to insert ‘sta’ and ‘dyn’ in the appropriate places for us?"
    "Vilem-Benjamin Liepelt")
 #s(slido-question
    "2018-09-26T09:49:00"
    "how does your approach compare with Quoted Staged Rewriting (best paper of GPCE 2017), which allows users to use type safe rewrite rules to modify generated code, mixing staging and rewriting in a simple way"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:50:00"
    "Have you measured how frex affects *compile* time?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T09:50:00"
    "How does the sparse-matrix benchmark compare to hand-tuned sparse matrix implementations?"
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-26T09:50:00"
    "In your matrix benchmark, was it statically known which entries are zero, but not what the value of the non-zero entries is?"
    "Philipp Schuster")
 #s(slido-question
    "2018-09-26T09:51:00"
    "Wouldn't it be better to use a dependently typed language like Agda or Coq so you can capture the laws in the type."
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:12:00"
    "How did you choose to present the paper in this way? What advantages or disadvantages do you think it has over a 'traditional' conference presentation?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:14:00"
    "Can this work be applied to give a practical recipe for building an optimizing query planner?"
    "Michael Arntzenius")
 #s(slido-question
    "2018-09-26T10:15:00"
    "Are finite maps a free extension?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:17:00"
    "Do graded monads come from some kind of graded adjunctions?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:31:00"
    "Why don't raising exceptions and dropping inputs have a distributive law?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:32:00"
    "Have you applied your work to settings where effects are modeled by adjunctions such as Call by push Value or polarized linear logic?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:34:00"
    "You've made for case that dropping a variable is an effect. Why is using one twice an effect?"
    "James Koppel")
 #s(slido-question
    "2018-09-26T10:35:00"
    "How does your approach compare with the standard approach of using polarised types to indicate strictness or laziness and mix the two?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-26T10:35:00"
    "In sequent calculus, an analogous problem arises when reducing stack abstraction against a value abstraction, with an analogous solution: favor either reducing value abstractions first or stack abstractions first, yielding either CBN or CBV. What's the relation between this work and sequent calculus"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:35:00"
    "Does laziness here mean call by name or call by need?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:36:00"
    "Monad transformers have their priority determined by their layering. How does this relate to your layering?"
    "Michael D. Adams")
 #s(slido-question
    "2018-09-26T10:36:00"
    "Can you give examples (besides the error/Maybe example) of how using monadic and comonadic tools together helps us write better programs? Better to me means faster, less errors, faster development, etc."
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:36:00"
    "How well do program fragments interpreted with each of the semanticses fit together? Can lazy and strict programs compose together well?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:36:00"
    "Would it be possible to have a language where you can choose the layering at each composition point, or do you have to remain consistent throughout a program?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-26T10:36:00"
    "What if I have multiple monads or multiple comonads? Is there a way to layer those?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:45:00"
    "Doesn't your assumption about finiteness imply that it's ok to identify sets and types?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:48:00"
    "Could you please speculate on applications?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:51:00"
    "There are different types of colour blindness. How did you find a good scheme that works for as many people as possible?"
    "Vilem-Benjamin Liepelt")
 #s(slido-question
    "2018-09-26T10:56:00"
    "Can your technique generalize to division? I.e. having a bijection 2 x A ~ 2 x B, construct a bijection A ~ B? Perhaps for infinite sets?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:56:00"
    "Instead of using \"partial bijections\" for g, why not \"complement\" it with an \"identity bijection\" on all the other elements?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:56:00"
    "Why do you use g inverse rather than g?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T10:59:00"
    "Is there a sense in which h-g+g=h?"
    "Stefan Muller")
 #s(slido-question
    "2018-09-26T11:00:00"
    "Does substraction have a nice universal property ?"
    "Guillaume Boisseau")
 #s(slido-question
    "2018-09-26T11:00:00"
    "Why infinite merge and not just size of g ?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T11:00:00"
    "Does this work on infinite bijections"
    "Michael D. Adams")
 #s(slido-question
    "2018-09-26T11:00:00"
    "I can't help thinking of comparing git commits... do you think there might be an application there?"
    "Beta")
 #s(slido-question
    "2018-09-26T11:01:00"
    "Would this work for anything less than or equal to omega as ordinal?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T11:01:00"
    "Have you proven this terminating in Agda? How hard would that be?"
    "James")
 #s(slido-question
    "2018-09-26T11:04:00"
    "Can you generalize to division?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:20:00"
    "Do you find that mathematical thinking teaches you to be more skeptical and pedantic and people perceive it as negativity or pessimism in real life?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:27:00"
    "Do you find many real world analogies to be ‘leaky’ and not suitable for categorisation? For example, dealing with spoken languages can have differing interpretations so a classification might seem reasonable but does not transcend people’s interpretation."
    "Colin")
 #s(slido-question
    "2018-09-26T08:28:00"
    "You criticized some high-school maths as \"algorithmic\" - long division? Do you disapprove of all algorithmic methods in high-school maths? What about Euclid?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:33:00"
    "For the Vicious Circle examples, you say that those with the most power should initiate breaking the cycle, and I agree. But this seems somewhat subjective. Do you have insight into how to apply mathematical thinking to arrive at this sort of judgment?"
    "Joey Eremondi")
 #s(slido-question
    "2018-09-26T08:37:00"
    "Is there an intuition behind why you drew the arrows going downward in the factors, or did it not matter to you at all whether they went up or down?"
    "Ron Garcia")
 #s(slido-question
    "2018-09-26T08:49:00"
    "Any application of abstract algebra to the world of Computer Science? Cryptography is an example. Any others?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:49:00"
    "What type of assignments do you teach your students? Or, put another way, how do they learn abstract thinking?"
    "Beta")
 #s(slido-question
    "2018-09-26T08:52:00"
    "In your experience, how well is this talk received by non-liberal people?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:52:00"
    "Aren't you forgetting to abstract over geographic location?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:53:00"
    "Do you have a concrete example of a virtuous circle between logic and empathy? Especially, an example of empathy influencing logic?"
    "Michael Arntzenius")
 #s(slido-question
    "2018-09-26T08:54:00"
    "How do you find the right level of abstraction? How do you know you are neither too high nor too low in the abstraction ladder?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:54:00"
    "This might sound sarcastic but, in a world of finite resources, is it really being intelligent not to be selfish?"
    "Anonymous")
 #s(slido-question
    "2018-09-26T08:55:00"
    "Have you attempted to present categorical topics such as limits and push outs in a social or philosophical settings?"
    "Stefan")
 #s(slido-question
    "2018-09-25T16:09:00"
    "Does the type system verify resource use of functions, or must they be \"correct by construction\"?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-25T16:09:00"
    "What other programs have you synthesized that you couldn't synthesize before?"
    "Justin")
 #s(slido-question
    "2018-09-25T16:09:00"
    "If the input lists were not sorted, can you get the synthesizer to sort them and then use the efficient algorithm?"
    "John Hughes")
 #s(slido-question
    "2018-09-25T16:10:00"
    "Could you give an example of algorithm synthesized beyond the scope of previous system"
    "Artem")
 #s(slido-question
    "2018-09-25T16:10:00"
    "What is the most complex program you've synthesized?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:17:00"
    "is box monadic, comonadic, or modal as in modal logic (which is neither)"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:18:00"
    "Could this approach be applied to tame the ! modality in linear types, or the modalities in the light linear lambda calculus?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-25T16:19:00"
    "Do your examples applications use antittone functions?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:20:00"
    "I’m surprised your ordering in sets is subset. Wouldn’t you like to take the ordering on elements into account? You might like the Singleton function to be monotonic on non-box types, for example."
    "John Hughes")
 #s(slido-question
    "2018-09-25T16:20:00"
    "Could this be generalized from monotone functions between posets to functors between categories?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:29:00"
    "Your extension is incompatible with overlapping instances. How easy is it to detect if its unsafe to use your extension in a given Haskell module? Do you need to check its imports?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:31:00"
    "Does the programmer need to provide any additional code for their typeclasses with your extension?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:31:00"
    "Are you proposing that all instances be bidirectional or just some?"
    "Jeremy Siek")
 #s(slido-question
    "2018-09-25T16:32:00"
    "Is your implementation available for GHC, and if so, what is keeping you from suggesting this as an official proposal?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-25T16:37:00"
    "Why stop at smooth manifolds? Could you extend to a higher order language if you used diffeological spaces?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:41:00"
    "What is required to prove that the choice of charts does not matter?"
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-25T16:41:00"
    "Would this approach be extendable to integration with, for example differential forms?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:42:00"
    "What about reverse mode differentiation?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T16:44:00"
    "Types ban erroneous programs. Can you provide a real-world example of such a program which your type system catches"
    "Artem")
 #s(slido-question
    "2018-09-25T14:02:00"
    "What CANNOT be understood about an object from its relationships?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:12:00"
    "What about the Yoneda Llama?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:16:00"
    "That requires Alpacategory theory"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:18:00"
    "What kind of type errors do you get with this setup?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:18:00"
    "But what is a profunctor?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:19:00"
    "Can you give a concrete example of use of this technique?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:20:00"
    "How do I know if Yoneda is lurking in my proof?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:20:00"
    "Is there any understanding of the 'minimal set' of relationships required to understand an object instead of all it's relationships?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:21:00"
    "Could you give a Real world examples of Profunctors?"
    "Vassil Keremidchiev")
 #s(slido-question
    "2018-09-25T14:36:00"
    "You said that you treat the rows of a table as a set. How do you get rid of traversing the entire table to check if the row existed?"
    "Mikaël Mayer")
 #s(slido-question
    "2018-09-25T14:36:00"
    "You mentioned that you use set semantics. Why this choice, when actual databases normally use bag semantics? And how to extend your work to bag semantics?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:37:00"
    "Do you use any heirustics when combining the relational lenses to ensure optimized SQL queries?"
    "Matt")
 #s(slido-question
    "2018-09-25T14:37:00"
    "Choosing to use minimal changes has a trade-off. It makes some things easier, like set difference. But computing the minimal change to a union expression (M \\cup N) _does_ require re-computing M and N. How did you decide that this particular tradeoff was the right one?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:38:00"
    "Can you get further speed-up by assuming that \"put\" is always performed after \"get\"? Or, can you make a version of \"get\" that computes some information required for the efficient \"put\"?"
    "Kazutaka Matsuda")
 #s(slido-question
    "2018-09-25T14:38:00"
    "Benchmarks against hand written SQL?"
    "Boyd Stephen Smith Jr.")
 #s(slido-question
    "2018-09-25T14:39:00"
    "Do Lens operations maintain ACID properties when interacting with databases? Remember, ACID properties guarantee reliability of database in the face of errors."
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:40:00"
    "How do you compare your incremental changeset to a commit in a version control system?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:40:00"
    "Do you consider data safety issues that come from concurrent edits in any particular way?"
    "Nick Lewchenko")
 #s(slido-question
    "2018-09-25T14:41:00"
    "Did you keep the type system from the original relational lenses?  How well did it work?"
    "Benjamin")
 #s(slido-question
    "2018-09-25T14:42:00"
    "How about join queries, in particular cyclic queries, eg {(a, b, c) | R(a, b) and R(b, c) and R(c, a)}?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-25T14:49:00"
    "Have you considered extending your work to regular tree languages?"
    "Michael Klein")
 #s(slido-question
    "2018-09-25T14:55:00"
    "Do you always get the representative of the equivalence class as result? Even when nothing has changed?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:57:00"
    "Do lenses on sums maintain which representative was chosen? Eg is there a tag, or is it a nondiscriminated union?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T14:57:00"
    "Would it be possible to do this for any of the subclasses of context free grammars? Could I synthesise my parser and pretty printer?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-25T14:57:00"
    "How hard would it be to reverse the canonizer, in order to reintroduce stripped whitespaces or comments, or keep the original order of fields?"
    "Mikaël Mayer")
 #s(slido-question
    "2018-09-25T14:59:00"
    "Does the kernel have that name because the canonizing function is a homomorphism? If yes, then do you have proofs of this in the paper?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:00:00"
    "Is it easy for programmers to know whether or not they have obtained intended lenses by the synthesis?"
    "Kazutaka Matsuda")
 #s(slido-question
    "2018-09-25T15:02:00"
    "Have you explored the theory of QREs?"
    "James")
 #s(slido-question
    "2018-09-25T15:02:00"
    "Besides perm and collapse, are there other primitives in QRE?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T15:05:00"
    "Awesome work!What language is this implemented it and how can I play around with it?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:08:00"
    "For getting the info back: that is no longer a standard lens, it's a symmetric lens, as that type of remembering requires puts going from right to left as well as left to right."
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:18:00"
    "Kmett's Lens takes notoriously long to compile. How long are the compile times when using generic-lens?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-25T15:21:00"
    "In the teachers-students example the order of application of lenses seems reverted. Is there a simple explanation for that?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-25T15:21:00"
    "How do you select for a particular constructor?"
    "Michael D. Adams")
 #s(slido-question
    "2018-09-25T15:21:00"
    "Does your approach work in conjunction with GADTs?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:21:00"
    "Nowadays some people like to enforce invariants in their tree structures by using GADTs. Do you have a story for generic traversals of such trees?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:23:00"
    "(note: you select for a particular constructor by (typed @\"name\"), as noted on a previous slide)"
    "Michael Klein")
 #s(slido-question
    "2018-09-25T15:24:00"
    "You said that the type-based lens is not allowed if there are multiple components of the type in a record. How does this work if you have a parameterized type like \"data D a = MkD a Int\" and you do not know yet whether the \"a\" will be instantiated to \"Int\" or not?"
    "Andres Löh")
 #s(slido-question
    "2018-09-25T15:24:00"
    "How does the performance compare with Template Haskell? Particularly considering that generics uses a binary tree representation of products and sums"
    "Anonymous")
 #s(slido-question
    "2018-09-25T15:24:00"
    "Can your static analysis of types work in the presence of two-level types/unfixed types (i.e.: Data types á la carte), where, for each sort of node, the type of their children is deffered?"
    "James Koppel")
 #s(slido-question
    "2018-09-25T15:25:00"
    "Could we have a traversal type that selects values of different types indexed over using type-level lists? Ie, heterogeneous traversals?"
    "Victor Miraldo")
 #s(slido-question
    "2018-09-25T15:25:00"
    "How does your approach deal with recursive types?  You had a mysterious back-pointer in your diagrams.  What about non-uniform recursion or mutual recursion?"
    "Simon PJ")
 #s(slido-question
    "2018-09-25T12:18:00"
    "What about customizable proof automation, ranging from auto to crush?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:20:00"
    "Can iris be used to model relational logic?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:20:00"
    "Could some of these techniques be useful in standard coq non separation logic"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:21:00"
    "Can I define a \"bad\" in any way tactic in MoSeL, or typechecker will catch it?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T12:21:00"
    "How easily can your work be generalized for substructural logics?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:33:00"
    "How does Mtac2 compare with Elaborator Reflection in Idris (Idris tactics written in Idris)?"
    "Boyd Stephen Smith Jr.")
 #s(slido-question
    "2018-09-25T12:34:00"
    "How well can MTac interact with Coq's builtin automation tactics? Things like `constructor` or `hypothesis` or `exists`, that need type information?"
    "Joey Eremondi")
 #s(slido-question "2018-09-25T12:34:00" "When can we use Mtac2?" "Anonymous")
 #s(slido-question
    "2018-09-25T12:35:00"
    "Elab in Idris also acts as a metaprogramming system for Idris (same goes for Lean’s tactic monad) Would it be possible to approximate Mtac2 to Template Coq?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:35:00"
    "How hard is it to convert existing ltac tactics to mtac2? Are there any examples for which this is not possible?"
    "Jesper")
 #s(slido-question
    "2018-09-25T12:38:00"
    "Could something like Mtac2 be used as a DSL for type checking or type inference?"
    "Andrew Kennedy")
 #s(slido-question
    "2018-09-25T12:40:00"
    "Could you define ttac monad as a mere transformer stack of M and the Writer monad?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-25T12:42:00"
    "How are the error messages?"
    "Anonymous")
 #s(slido-question "2018-09-25T12:44:00" "Is it fast?" "Anonymous")
 #s(slido-question
    "2018-09-25T12:57:00"
    "Your approach seems to be inherently big step (relating state to result). Can you deal with concurrency, non-termination, or input/output as those often use small step semantics?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:59:00"
    "Can the techniques you described be translated to a monadic setting?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T12:59:00"
    "Did you try it with monads or other structures? How critical was to have the control flow explicit and known (one feature that arrows provide) to make this work?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-25T13:00:00"
    "Have you considered using a meta language the can internalize parametricity results like ParamDTT presented one year ago?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:00:00"
    "Can arrow induction and parametricity be combined?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-25T13:00:00"
    "Can you give an example of analysis where your approach does not work? Could abstract interpretation have more steps, that do not match concrete interpretation? Or vise versa"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T13:15:00"
    "What exactly is equivalence between data structures? Is it equivalence of behaviours of some \"essential\" functions? Where is the boundary between essential functions (like length of lists/vectors) and the library?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T13:17:00"
    "Does your system prove for free that binary addition is equivalent to the addition on Peano natural numbers?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:18:00"
    "Does this framework work for coinductive datatypes?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:20:00"
    "Agda has implemented the full cubical type theory for a while, which seems to support all the features mentioned in this talk without being blocked. Is there any reason why you chose a different path?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:20:00"
    "What is the relation to proof irrelevance?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:20:00"
    "Does univalent parametricity support using non-isomorphic relations? If not, do we lose applications to general type abstraction?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T13:20:00"
    "If I write a function on standard Nats, I get a function on BinNats for free. But this free version won't be very performant, right? How much work do I have to do to get the efficient version of the function?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-25T13:21:00"
    "Why do we prove decidable equality rather than that the type in question is a h-set?"
    "James")
 #s(slido-question
    "2018-09-25T13:21:00"
    "You went quickly over the explanation of why ordinary parametricity is  inadequate. I didn't follow. Can  you please explain in more detail?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T13:21:00"
    "What is the runtime overhead of using automatically transported functions?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:28:00"
    "Should we phase out in-person conferences as a primary publication vehicle in CS?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:33:00"
    "How about optimizing conference venue location to minimize flights for expected participants?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:34:00"
    "Would making conference attendance non mandatory for publication be a feasible step to reduce emissions?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:35:00"
    "Will NSF grants cover that fees?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:36:00"
    "Can we also institute remote presentations?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:37:00"
    "Isn't individual consumption dwarfed by commercial / industrial? Do market based solutions have a chance of succeeding when this requires political intervention and companies have a larger influence here than individuals (and are only concerned with making profit)?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:38:00"
    "How realistic are the carbon offset prices you show? Is there any way to tell whether they actually work?"
    "Jesper")
 #s(slido-question
    "2018-09-25T11:39:00"
    "How well does carbon offsetting scale? Is it feasible to offset all carbon emissions?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-25T11:40:00"
    "How is the average CO2 production for Americans calculated? Is it budgeting individual choices only, or also the difficult-to-avoid participitation in large systems like say global trade?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:40:00"
    "Should we have the option for a corporate sponsor who can advertise at icfp while contributing most or some of the fee to an climate offset company?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:42:00"
    "Is there perhaps hiding a global optimization problem in this? Like minimize travel when tweaking colocation? Using actual travel data from participants to simulate this?"
    "Atze Dijkstra")
 #s(slido-question
    "2018-09-25T09:49:00"
    "If the priorities form a loop, does it pose any kind of problems in the system?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-25T09:50:00"
    "Would it be feasible to have *contextual* thread priorities in the type system? E.g instead of ruling out syncing on a lower priority thread, having the type system enforce that the synced upon thread *inherits* the priority of the waiting thread, if higher?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T09:52:00"
    "You gave  an example where priority inversion affected the Mars Rover. What went wrong? How would your type system catch it? How would you alter the program so it would type in your system?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T09:53:00"
    "Have you looked at Kobayashi's type systems for pi-calculus guaranteeing liveness and time-boundedness? https://doi.org/10.1006/inco.2002.3171"
    "Anonymous")
 #s(slido-question
    "2018-09-25T09:53:00"
    "It seems that this system allows any thread to spawn new threads at any priority. Doesn't that break the guarantees about priority inversion and responsiveness?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T09:53:00"
    "If there are too many high-priority threads, could low-priority threads never get time?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T09:53:00"
    "How does your system handle starvation of low priority threads?"
    "Spencer Florence")
 #s(slido-question
    "2018-09-25T09:53:00"
    "Would it be possible to avoid priority inversion by \"tainting\" the low priority one to be a high one?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:07:00"
    "Are there other back-ends in addition to OpenCL, or are others planned?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:07:00"
    "Is it an instance of partial evaluation? In particular, could static interpretation be presented as a rewriting relation, included in the usual small-step semantics?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:08:00"
    "Is static interpretation a form of defunctorization as done by MLton?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:09:00"
    "How much of the theory in this paper is specific to Futhark?  How far is it applicable to other ML-like module systems in general?"
    "Eijiro Sumii")
 #s(slido-question
    "2018-09-25T10:15:00"
    "Is the implementation in the compiler extracted from the Coq implementation?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:16:00"
    "Can static interpretation be done partially, when not all inner pieces are fully defined? If so, how surface language and semantic objects are safely linked?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T10:18:00"
    "The rules look very complex, as do those for ML. What is the source of complexity? How could we make it go away?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T10:30:00"
    "If \"finitary\" means \"only sees a finite number of different type instances\", then what about polymorphic recursion?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:34:00"
    "Would this work for a compiler that does not have access to the whole program, i.e., separate compilation?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:36:00"
    "(The question on polymorphic recursion can usually be solved by noticing that we don't care about all the information in a type, but some finitary aspect of it like \"word size in my data representation\", so even infinite sets of types can usually be mapped to finitely many representation classes.)"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:37:00"
    "With this optimization, how efficient code does your compiler produce, compared to other implementations of ML-like languages?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:37:00"
    "I take it your method works only on closed programs. How does it compare with aggressive beta reduction of type abstraction and instantiation?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T10:38:00"
    "(The sli.do interface for questions is really less pleasant to use than Slack; no way to edit question, to easily respond to them online, etc.)"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:38:00"
    "Do you have some benchmarks for your optimisation?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:41:00"
    "Haskell’s type classes and qualified types seems to address the same problem of finitary polymorphism.  Do you see any similarities to your system?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T10:43:00"
    "(Comment on polymorphic recursion: it's still quite possible to get infinite sets. In .NET generics, which used specialization up to representation,we had this issue.)"
    "Andrew Kennedy")
 #s(slido-question
    "2018-09-25T10:44:00"
    "Beta-reduction duplicates code. This optimization doesn't."
    "Anonymous")
 #s(slido-question
    "2018-09-25T11:01:00"
    "What if the voting execution glitches?"
    "Elsman")
 #s(slido-question
    "2018-09-25T11:02:00"
    "Don’t you need to keep track of whether faults are independent or not? E.g. using two identical sensors increases certainty, but using the output of one sensor twice does not."
    "John Hughes")
 #s(slido-question
    "2018-09-25T11:05:00"
    "Two questions. 1) Have you tried to extend this to accommodate continuous distributions? 2) What about correlated failures? These are obviously challenging to capture but seem quite essential to properly account for real-world failures."
    "Ben")
 #s(slido-question
    "2018-09-25T11:05:00"
    "Wow. Very nice. So which satellites have been designed using Haskell?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T11:05:00"
    "How your system is used in real time environment without any GCs?"
    "Vassil Keremidchiev")
 #s(slido-question
    "2018-09-25T08:18:00"
    "You refered to Moores's Law. The empirical evidence for it is clear, but what (if any) is the reason why it holds?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-25T08:20:00"
    "How does the energy put into manufacturing domain specific hardware compare to the efficiency savings of using it?"
    "Joey Eremondi")
 #s(slido-question
    "2018-09-25T08:21:00"
    "What about the loss of flexibility when we move to hardware? Instead of being able to make a new project in 3 months with one programmer and the only costs being food and power, it takes years to prototype hardware. Isn't that a death knell for PL research?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T08:24:00"
    "Is there a tension between standardisation of libraries and specialisation of designs?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-25T08:27:00"
    "You mentioned (lack of libraries) having to purchase IP. Do you have a sense for how much the various IP hurdles impedes innovation in this area?"
    "SteveS")
 #s(slido-question
    "2018-09-25T08:29:00"
    "Main issue for mainstream adoption of Verilog  alternatives is languague-to-bitstream flow requiring traditional HDLs -- which means reliance on them for debugging, timing optimization, and interoperability with vendor's IP blocks. Has there been any work addressing these problems you'd recommend?"
    "Matt P. Dziubinski")
 #s(slido-question
    "2018-09-25T08:31:00"
    "Why would you verify in Verilog using SMT/SAT instead of verifying using theorem proving and verified compiling to Verilog?"
    "Ed")
 #s(slido-question
    "2018-09-25T08:33:00"
    "Why do you need @classmethod annotations in your Python-like language?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-25T08:33:00"
    "What do you think synchronous dataflow programming languages for hardware design, like synchronous lucid?"
    "Fengyun")
 #s(slido-question
    "2018-09-25T08:37:00"
    "Bluespec is a commercial  Haskell inspired hardware description language. Chisel is based on imperative Scala. Could you compare the two?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T08:38:00"
    "What is your opinion of the (now defunct) programming language Occam? Which was used for both hardware and software descriptions and proofs of correctness."
    "Ed")
 #s(slido-question
    "2018-09-25T08:40:00"
    "How would you verify timing properties of designs?"
    "Ed")
 #s(slido-question
    "2018-09-25T08:42:00"
    "Have you thought of parallel harware as Petri nets instead of FSMs?"
    "Vilem-Benjamin Liepelt")
 #s(slido-question
    "2018-09-25T08:47:00"
    "Are DSLs the closest we have to a library ecosystem for hardware design?"
    "Michael Klein")
 #s(slido-question
    "2018-09-25T08:51:00"
    "Hardware companies rely on selling designs services and licensing IP. This model is directly incompatible with open source and standard libraries, code reuse, etc in many ways. Do you have a vision to resolve these conflicts in a commercially viable way? Is RISC-V pioneering this?"
    "Ed")
 #s(slido-question
    "2018-09-25T08:52:00"
    "Is there any current work on functional DSLs for analogue computers?"
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-25T08:53:00"
    "How do you synthesise control for schedules produced by  Halide approach?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T08:54:00"
    "A bug in hardware is much harder to fix than a bug in software. Shouldn't our hardware description languages also be hardware verification languages?"
    "Jesper")
 #s(slido-question
    "2018-09-25T08:54:00"
    "How do you test and/or verify programs in Halide?"
    "Julia Belyakova")
 #s(slido-question
    "2018-09-25T08:55:00"
    "Are hardware DSLs composable?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T08:56:00"
    "Even with better tools, wouldn’t manufacturing costs be a major barrier to innovation in hardware design?"
    "Anonymous")
 #s(slido-question
    "2018-09-25T08:57:00"
    "Are you familiar with Feldspar and the later work of Mary Sheeran on DSLs for essentially creating hardware for digital signal processing?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-25T08:58:00"
    "How long did it take to design and physically construct the CGRA after you have chosen your tools?"
    "Stefan")
 #s(slido-question
    "2018-09-25T09:00:00"
    "If we see a processor as an interpreter, is there a role for partial evaluation in moving between processors and custom hardware?"
    "John Hughes")
 #s(slido-question
    "2018-09-24T15:49:00"
    "Do you plan to continue lections on Channel9 (theory of categories, etc)?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:51:00"
    "The basic building-block identifies a space with its tangent space, this assumption doesn't generalise to more sophisticated situations. Is this a limitation?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:54:00"
    "Can this formalism be extended to obtain an estimate in what region the function is actually well described by its value and derivative (which can be used for step size in Gradient Descent)?"
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-24T15:56:00"
    "Your \"join\" operation (downwards triangle) depends on addition. Can this structure be made more general, not depending on numbers?"
    "Jeremy Gibbons")
 #s(slido-question
    "2018-09-24T15:58:00"
    "Regarding matrix extraction & performance: Certain optimization algorithms only need the matrix-vector product (for a given vector) instead of the entire matrix. Is there a  high-performance solution you'd recommend?"
    "Matt D.")
 #s(slido-question
    "2018-09-24T15:58:00"
    "How could this technique support offloading computation to a GPU?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T16:02:00"
    "Have you investigated the relation between the structure captured by your lolipop morphisms and additive categories?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T16:04:00"
    "The isomorphism between a and (a ⤳ s) only holds in in a Hilbert space (Riesz representation theorem). Can these ideas also be implemented without a scalar product?"
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-24T16:04:00"
    "You mention incremental computation. Under what notion of changes? For example, what are the changes allowable on sum types?"
    "Michael Arntzenius")
 #s(slido-question
    "2018-09-24T16:22:00"
    "What properties to you require from an instance of MonadInfer to get meaningful results?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T16:25:00"
    "Have you considered barring your work on extensible-effect libraries rather than Monday trans"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T16:25:00"
    "Does using a monadic API have a negative impact on the ways you can process models defined in the API?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T16:25:00"
    "Can you implement a popular DSL such as PyMC3 (completely)?  How do you exploit algebraic  properties of distributions?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-24T16:25:00"
    "Rather than monadic transformers"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T16:42:00"
    "Does Scibior et al.'s POPL 2018 paper also get all green checkmarks?"
    "Chung-chieh Shan")
 #s(slido-question
    "2018-09-24T16:43:00"
    "How does your language differ from that of Borgstrom et al? Did you just happen to find the trick to proving commutativity, or does your language have some extra feature to enable this?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-24T16:43:00"
    "Have you thought about using gradual types to also allow static checking?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T16:47:00"
    "Commutativity seems natural when interpreting programs as operating (monadically) on distributions.  Can your paper be understood as requirements on samplers to statistically correctly implementing such a denotational semantics?  Are “all” samplers correct?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-24T16:47:00"
    "Could equivalence be proved using a method such as bisimulation rather than induction on the length?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T16:48:00"
    "Could you give some interesting example in your language where the programs are equivalent in isiolation but not so in a context?"
    "Eijiro")
 #s(slido-question
    "2018-09-24T16:49:00"
    "Are sets of provably-equivalent programs same for small and big-step semantics?"
    "Julia")
 #s(slido-question
    "2018-09-24T17:03:00"
    "How many LoCs are students expected to write (tests+game+etc), and how many students are in a group?"
    "Beta")
 #s(slido-question
    "2018-09-24T17:04:00"
    "You said students do not have prior programming experience. Do you provide them with example games to investigate the code in the first place?"
    "Julia")
 #s(slido-question
    "2018-09-24T17:04:00"
    "Much of what you describe (good as it is) would be equally applicable to building games in Javascript or C++.  How important is it to you that you are using Haskell?   And why?"
    "Simon PJ")
 #s(slido-question
    "2018-09-24T17:04:00"
    "Is this system open source? How extensible is it?"
    "Ed")
 #s(slido-question
    "2018-09-24T17:05:00"
    "Do you try implicitly or explicitly introduce the concept of FRP in you classes."
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T17:05:00"
    "How do you teach students to deal with the more complex type errors in Haskell?"
    "Jesper")
 #s(slido-question
    "2018-09-24T17:05:00"
    "Is it possible for students to explore with these tools beyond the constraints of course specified assignments?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T17:06:00"
    "You didn’t use QuickCheck for testing... I wonder why not?"
    "John Hughes")
 #s(slido-question
    "2018-09-24T17:06:00"
    "How do you tell how engaged students are? How do you know if the good programming habits you teach with this assignment stick after the course ends?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T17:07:00"
    "Are there evaluation to compare the effectiveness of this methodology with the more traditional CS education curriculum?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T17:07:00"
    "What are the most common problems students experience while using this framework?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T17:07:00"
    "I teach 1st year OO programming using C# where students, barely, implement two 80s retro games in 16 intensive weeks. Will I still have a job after showing them your approach?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-24T17:07:00"
    "How do you give students feedback on programming style and design, beyond testing?"
    "Chung-chieh Shan")
 #s(slido-question
    "2018-09-24T17:08:00"
    "Did the original 12 week outline stay the same after incorporating these assignments? What had to be tweaked in the pedagogy to accommodate the game building."
    "Zac Slade")
 #s(slido-question
    "2018-09-24T17:08:00"
    "You said that few tools that measure code quality and give feedback to students.  That's a challenging task!  Are excellent tools available in ANY language?"
    "Simon PJ")
 #s(slido-question
    "2018-09-24T17:09:00"
    "How do you enforce that student code should run in a decent time limit to ensure real-time games?"
    "Mikaël Mayer")
 #s(slido-question
    "2018-09-24T17:09:00"
    "How do you incorporate principles of structuring programs in the design of assignments?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T17:09:00"
    "Do you have some kind of interactive system (such as typed holes) to help students develop their code?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T17:09:00"
    "How do students learn *why* code style choices are important not just what the correct style is?"
    "Ed")
 #s(slido-question
    "2018-09-24T17:12:00"
    "The games are impressive. How many hours do students spend on it on average and is it reasonable? (The “norm” in my university was 1.5 h per week per ECTS credit)"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:16:00"
    "I'm still worried about performance in large probabilistic models. Can you use memoization to reduce replay overhead?"
    "Chung-chieh Shan")
 #s(slido-question
    "2018-09-24T14:20:00"
    "How replay can be implemented without built-in shift/reset or through an extra  transformation pass?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:20:00"
    "The implementation is based on an unsafe cast - is there a way to avoid such a cast?"
    "Elsman")
 #s(slido-question
    "2018-09-24T14:21:00"
    "Could you avoid redoing native effects and long-running computations by wrapping them in some caching construct?"
    "Philipp Schuster")
 #s(slido-question
    "2018-09-24T14:21:00"
    "can your optimizations for \"thermometer continuations\" apply to ocaml's delimcc library?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:22:00"
    "Do you have other examples besides N-queen?"
    "Julia")
 #s(slido-question
    "2018-09-24T14:23:00"
    "Can this approach support Racket-style prompt tags?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:24:00"
    "Can this be used at the same time as native language exceptions?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:40:00"
    "Are the restrictions 1 and 2 related to the split contexts used for some linear dependent type systems?"
    "James")
 #s(slido-question
    "2018-09-24T14:42:00"
    "Does your approach apply to both, CoC and CiC?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T14:43:00"
    "What are some examples of applying your translation to a complete small program?"
    "Chung-chieh Shan")
 #s(slido-question
    "2018-09-24T14:43:00"
    "Reply to Artem: our language has inductive types, and the approach should extend to polymorphic languages."
    "Youyou Cong")
 #s(slido-question
    "2018-09-24T14:43:00"
    "Do you think your approach will extend to dependent algebraic effects and dependent effect handlers?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:44:00"
    "Are there any issues with call by name versus call by value?"
    "Julia")
 #s(slido-question
    "2018-09-24T14:45:00"
    "Algebraic effects are more powerful; to support them we need multi-prompt shift/reset."
    "Youyou Cong")
 #s(slido-question
    "2018-09-24T14:47:00"
    "Reply to Julia: I’m trying CBN shift and reset right now; they require a more complicated type system due to the special treatment of continuation variables."
    "Youyou Cong")
 #s(slido-question
    "2018-09-24T14:53:00"
    "How is \"aligning\" defined? Is it a primitive?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:53:00"
    "Why did you decide to add implicits in your language? Could you have passed the restrictions to the correlate function directly?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T14:53:00"
    "You claim direct-style, but your correlate-from-yield look very monadic to me. How is it direct style?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T14:55:00"
    "Could this language be embedded in a language such as Haskell?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:01:00"
    "You talk about suspending and resuming computations. Are these cooperatively multiplexed onto one OS thread, or genuinely parallel?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:06:00"
    "Most recent seemed difficult to write and confusing. How hard is it to add new restrictions and what's the hardest part to you?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:06:00"
    "How are sort-merge and various hash or discrimination  joins done? Isn’t there some clean-up/Post processing required at end of streams?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-24T15:06:00"
    "Are all of the measurements on your microbenchmarks totally precise? If not, what’s the error like on them?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:07:00"
    "Can you do something like leapfrog trie join with your framework?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T15:09:00"
    "easy to write == direct style :)"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:11:00"
    "Where is environment in the STLC example?"
    "Julia")
 #s(slido-question
    "2018-09-24T12:12:00"
    "How would you encode linear or otherwise substructural variables?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:14:00"
    "Can the system deal with \"non algorithmic\" typing rules: e.g. subsumption and transitivity of subtyping?"
    "Abel")
 #s(slido-question
    "2018-09-24T12:19:00"
    "Does the use of HOAS allow you to create ill-formed terms, or does Lambda Prolog have some feature to prevent this?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-24T12:19:00"
    "What are Makam's specific constructs? Are they implemented as extensions to lambda-prolog?"
    "Beta")
 #s(slido-question
    "2018-09-24T12:20:00"
    "You only describe types. Can you use Lambda Prolog to describe reductions? To prove preservation and progress?"
    "Philip Wadler")
 #s(slido-question
    "2018-09-24T12:21:00"
    "Have you tried implementing a bidirectional system? Can you comment on the importance of information flow in the type system?"
    "James")
 #s(slido-question
    "2018-09-24T12:24:00"
    "A nice feature of pltredex is the test case generation aspect. How does it work here ?"
    "Gabriel Radanne")
 #s(slido-question
    "2018-09-24T12:38:00"
    "What are the limitations of the framework? Is there a way to express recursive types, polymorphism?"
    "Julia")
 #s(slido-question
    "2018-09-24T12:40:00"
    "Can you construct a similar universe for linear or otherwise substructural variables?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:41:00"
    "This works seems very similar to Marcelo Fiore's work on abstract syntax with binding. Do you know of specific points of comparison?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:44:00"
    "Can you easily compose two different syntaxes in your framework?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:47:00"
    "Does the framework allow to represent a syntax with telescopes for example for a dependently typed language"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:56:00"
    "What are this differences between what you present here and quasi quoting in Haskell?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:57:00"
    "When will we get TLMs in a good language (like Haskell)?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T12:57:00"
    "Can the choice of TLM be directed by types without introducing confusion?"
    "Garrett")
 #s(slido-question
    "2018-09-24T13:02:00"
    "Can templates be constructed dynamically?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:02:00"
    "Could notations be dependently-typed? e.g. for length-indexed vectors in Coq. Would inference be affected?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:02:00"
    "Is the capture avoidance configurable? Could I chose for an expansion to introduce a new name available in splices?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T13:03:00"
    "What are the error messages like? Is there the scope for library writers to put in custom error messages that talk about the surface syntax?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-24T13:04:00"
    "How well do these notations compose? For example, when one language contains another as a sublanguage."
    "James")
 #s(slido-question
    "2018-09-24T13:04:00"
    "Can TLM expansions bind variables in splices? If so, do nested TLM invocations work like Scheme with respect to capture?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:05:00"
    "Can you talk a little bit about the typing part. Where does the expansion happens during OCaml typechecking ?"
    "Gabriel Radanne")
 #s(slido-question
    "2018-09-24T13:05:00"
    "What about nesting extensions? Using your example, for example $ipv4 inside $url?"
    "Vadim Zaliva")
 #s(slido-question
    "2018-09-24T13:06:00"
    "Following Jennifer's q on error messages: does capture-avoiding substitution gets in the way here?"
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T13:06:00"
    "Programmable/variable concrete syntax is credited with being one of reasons for the nonsuccess of Algol 68.  Which considerations are in favor of programmability by library designers of liberal syntax as in TLM?"
    "Fritz Henglein")
 #s(slido-question
    "2018-09-24T13:06:00"
    "You focus on literals. Why? Don't other macros have the same problem?"
    "Michael D. Adams")
 #s(slido-question
    "2018-09-24T13:07:00"
    "Does open notation nest, and if so, does that reintroduce ambiguity?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:11:00"
    "Presumably the parser can do arbitrary IO. Does that mean we can abuse the mechanism to get type providers?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:13:00"
    "Why did the top question not get answered?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T13:31:00"
    "During your translation, is there any inherently performance-degrading steps? Say, exponential time or space penalties."
    "Artem Pelenitsyn")
 #s(slido-question
    "2018-09-24T13:32:00"
    "When would one use AAM, when ADI? Why translate between them?"
    "Sebastian")
 #s(slido-question
    "2018-09-24T13:33:00"
    "Are the translation steps mechanized or done by hand?"
    "Anonymous")
 #s(slido-question "2018-09-24T09:32:00" "Can we make the warmer?" "Anonymous")
 #s(slido-question
    "2018-09-24T09:45:00"
    "Any thoughts about Build Systems for directed (possibly cyclic) graphs (\"consistency restoration\" - an application of Bidirectional Transformations)?"
    "Jeremy Gibbons")
 #s(slido-question
    "2018-09-24T09:46:00"
    "How do you handle dependencies like Latex that contain cycles?"
    "Michael D. Adams")
 #s(slido-question
    "2018-09-24T09:47:00"
    "This sounds like callback hell. Why isn't it?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T09:50:00"
    "The schedulers and rebuilders table has empty rubrics. Are these opportunities, or tarpits?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T09:52:00"
    "If a Scheduler is merely a function from Rebuilders to Build Systems, then a Build System is almost merely a (Scheduler, Rebuilder) pair. Right?"
    "Jeremy Gibbons")
 #s(slido-question
    "2018-09-24T09:52:00"
    "Is it possible to combine schedulers? For example, is there a suspending topological scheduler?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T09:56:00"
    "Can you describe what a frankenbuild is and how BSalC approaches then?"
    "Patrick Thomson")
 #s(slido-question
    "2018-09-24T10:01:00"
    "Lazyness involves sharing, do you plan to address that as future work ? That perhaps opens the door to test memory leakage ?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-24T10:02:00"
    "Which take does the prelude use and why?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:07:00"
    "Is \"f x = undefined\" strict? What answer does your system give, and is that the \"right\" one?"
    "Jenny Hackett")
 #s(slido-question
    "2018-09-24T10:07:00"
    "How critical and good does the generation of contexts need to be to trigger bugs? What’s your experience with that ?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-24T10:10:00"
    "Could this strictness analysis be done statically instead of by dynamic testing?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:13:00"
    "StrictCheck can help me check my strictness specification. Is there a story for how we should be documenting strictness specifications for users? Do we expect them to read the Haskell implementations of the strict check properties?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T10:13:00"
    "Does your use of unsafePerformIO make it harder to parallelize these tests?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:15:00"
    "Can this model of laziness as \"demanding\" parts of values from a context be related to a continuation-passing-style representation of values?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:15:00"
    "What's involved in moving from 'testing' to 'proving'? What's going to be hard?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:15:00"
    "On the suggestion that one can compare the magnitude of strictness of two functions, what do you mean. I can only think of counting how many arguments are."
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:18:00"
    "space use can depend on evaluation order. Have you thought about extending this to test evaluation order?"
    "John Hughes")
 #s(slido-question
    "2018-09-24T10:21:00"
    "Can we stop having the sli.do slide during questions?"
    "Garrett")
 #s(slido-question
    "2018-09-24T10:24:00"
    "How can we convince you to do this in Haskell? :)"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:26:00"
    "It seems that to get the interactive, user experience enhanced we need to conceive the compiler stack in an incremental fashion from the beginning. If we want this for Haskell, then do we need  to rewrite it in similar ways as you did with Ocaml or  we can spare that effort somehow?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-24T10:27:00"
    "Did you find the LSP protocol well-suited to functional languages? Do you have ideas for a better protocol that could be common between functional languages?"
    "Joey Eremondi")
 #s(slido-question
    "2018-09-24T10:29:00"
    "Any plans to adapt the Merlin infrastructure to support other programming languages?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:30:00"
    "Were there any challenges related to using LSP with FP languages specifically, or is designed with both imperative and FP languages in mind?"
    "Matthías Páll Gissurarson")
 #s(slido-question
    "2018-09-24T10:30:00"
    "How do we deal with mass refactoring, where multiple files are open, several currently have type errors and one or two have syntax errors.  But, the session started will a clean, complete build.Specifically, with this much \"pariality\", can you avoid reporting out-of-date type information?"
    "Boyd Stephen Smith Jr.")
 #s(slido-question
    "2018-09-24T10:31:00"
    "Will you be merging your changes to the OCaml lexer and parser back to mainline?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:32:00"
    "Should all lexers/parsers be written in your pure style?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:37:00"
    "Can the synthesized parser recovery logic be used to provide user feedback to help provide more productive or helpful parser errors?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T10:37:00"
    "How do you make sure that your language server runs in the same context (compiler version, dependency versions, compiler flags, ...) as your build?"
    "Philipp Schuster")
 #s(slido-question
    "2018-09-24T10:37:00"
    "Your recovery never deletes tokens?"
    "John Hughes")
 #s(slido-question
    "2018-09-24T10:37:00"
    "Could any of the incremental parsing you are doing in language server be re-used by editors for things like syntax highlighting, indentation, etc?"
    "Vadim Zaliva")
 #s(slido-question
    "2018-09-24T10:38:00"
    "What recommendations do you have for language designers in order to enable better/faster/easier language-server support for their future language(s)?"
    "Ross Tate")
 #s(slido-question
    "2018-09-24T10:39:00"
    "How do you resolve dependencies between files? Since the server is stateless, doesn't it need the entire workspace as input?"
    "Sebastian")
 #s(slido-question
    "2018-09-24T10:39:00"
    "How should functional language syntax and semantics be designed in the future to better accomodate good interactive tooling like Merlin?"
    "David Christiansen")
 #s(slido-question
    "2018-09-24T10:39:00"
    "Do you have a significant performance loss due to the purity you introduced?"
    "Artem")
 #s(slido-question
    "2018-09-24T10:39:00"
    "How performant is merlin relative to other implementations? Does a purely functional design create greater memory overhead or less relative to other LS \"architectures\"?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:39:00"
    "Can recovery step lead to incorrect programs executing at runtime?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:39:00"
    "Rather than inserting e.g. empty strings, couldn’t you have added hole expressions (which the type checker would then assign a type variable as type)?"
    "John Hughes")
 #s(slido-question
    "2018-09-24T10:54:00"
    "Can you build hollow shapes in CAD?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:55:00"
    "Does a model need to consist og only one part?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:56:00"
    "Many STL files are not solid hulls as they contain unintended gaps between faces. How do you handle models that are not solid during the decompilation?"
    "Zac Slade")
 #s(slido-question
    "2018-09-24T10:57:00"
    "It seems like going from constructive solid geometry, which sounds like what you’re doing with CAD, to voxels would be an easier approach than going to a mesh. Have you considered this as “output” from your compiler?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:57:00"
    "Can this be translated over for CNC use rather than 3d printing?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T10:58:00"
    "Are there interesting uses for the decompiled mesh when you also have access to the original CAD specification?"
    "Eric Mertens")
 #s(slido-question
    "2018-09-24T10:59:00"
    "Why is the compiler doing small-step rewrites instead of \"big-step\" compilation?Compile(cad-union(c1, c2)) := mesh-union(Compile(c1),Compile(c2))"
    "gasche")
 #s(slido-question
    "2018-09-24T10:59:00"
    "You mentioned that CAD softwares are not compatible  - does this mean you can decompile to different standards?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T11:00:00"
    "The special union on meshes clear, but why do we need a special union for solids?"
    "Samuel Gélineau")
 #s(slido-question
    "2018-09-24T11:01:00"
    "I can understand semantics preservation assuming exact real arithmetic. What happens with approximate (fixed- or floating-point) numbers? (Overlaps Slade's q.)"
    "Jeremy Gibbons")
 #s(slido-question
    "2018-09-24T11:03:00"
    "Are there any fundamental mathematical reasons for going via meshes at all, or is this just a legacy thing that's needed for current 3D printers? It seems that most transformations would actually be _easier_ directly on a CAD representation, and determing whether a point is inside would be easy too."
    "Justus Sagemüller")
 #s(slido-question
    "2018-09-24T11:07:00"
    "What's an example of something you can almost decompile but not quite?"
    "Chung-chieh Shan")
 #s(slido-question
    "2018-09-10T19:12:00"
    "Is this a good question?"
    "Anonymous")
 #s(slido-question
    "2018-09-10T19:13:00"
    "How many questions will we ask to test this out?"
    "Anonymous")
 #s(slido-question
    "2018-09-10T19:16:00"
    "How much wood could a woodchuck chuck if a woodchuck could chuck wood"
    "Anonymous")
 #s(slido-question
    "2018-09-10T19:16:00"
    "Is it easy to use a phone to ask questions?"
    "Anonymous")
 #s(slido-question "2018-09-10T19:16:00" "你們會不會講中文" "Anonymous")
 #s(slido-question "2018-09-10T19:46:00" "Are we there yet?" "Anonymous")
 #s(slido-question "2018-09-10T19:47:00" "Is it much farther?" "Matthew")
 #s(slido-question
    "2018-09-10T20:56:00"
    "Can fairytales come true if you're young at heart"
    "Anonymous")
 #s(slido-question
    "2018-09-15T07:30:00"
    "How did you prepare for your performance of \"Nessun Dorma\"?  H's?ow does your approach differ from Pavarotti's?"
    "Mitch Wand")
 #s(slido-question
    "2018-09-18T10:08:00"
    "Three ants are placed at the vertices of a triangle and each randomly picks an edge and starts walking along it. What is the probability that they meet?"
    "Robby Findler")
 #s(slido-question
    "2018-09-19T14:35:00"
    "How much wood could a woodchuck chuck if a woodchuck could chuck wood.?"
    "Garrett")
 #s(slido-question
    "2018-09-23T06:03:00"
    "Is this the link for viewing video as well?"
    "Blake Elias")
 #s(slido-question
    "2018-09-23T06:06:00"
    "What places do you recommend for dinner? Something with nice veggie options?"
    "Anonymous")
 #s(slido-question
    "2018-09-23T17:39:00"
    "Can types speak the truth ?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-24T07:55:00"
    "Will there be a video record of the talks we can access on the web?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T08:06:00"
    "How do we get a working internet connection?"
    "Beta")
 #s(slido-question
    "2018-09-24T08:51:00"
    "Can U1 ~ U2 be thought of as unifiability? And thinking that way, does it make sense to name ?s like unification variables?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T08:56:00"
    "I'd like to use dynamic Haskell programs from Agda, could gradual types help here as well? How would you deal with programs that \"go wrong\" by not terminating?"
    "Jesper")
 #s(slido-question
    "2018-09-24T08:59:00"
    "If you have types representing effects, what is the intuition behind  consistency? Matching exactly the effects or a subset of them?"
    "Alejandro Russo")
 #s(slido-question
    "2018-09-24T09:02:00"
    "What are the requirements to the gradual-intermediate-language? It seems to be a mega-language containing all: static, dynamic, and extra-runtime-checks."
    "Julia")
 #s(slido-question
    "2018-09-24T09:07:00"
    "Does complexity of the gradual language depend on the \"distance\" between languages? Or rather the complexity of the \"maximal\" language?"
    "Julia")
 #s(slido-question
    "2018-09-24T09:08:00"
    "Many languages have a connection to lower-level languages, like C bindings. Do you see a connection between different-language linking and Gradual Typing?"
    "Beta")
 #s(slido-question
    "2018-09-24T09:12:00"
    "Scala allows you to type program fragments that go wrong, but you cannot use these fragments. Does this mean there's no hope for a gradually typed Scala?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T09:12:00"
    "Some properties are harder to check dynamically than statically (parametric polymorphism, termination, etc.).  To what extent can gradual typing help?"
    "Eijiro")
 #s(slido-question
    "2018-09-24T09:13:00"
    "There are some approaches promising embedding of dynamic language in Haskell. Could Haskell be our gradually typed language?"
    "Anonymous")
 #s(slido-question
    "2018-09-24T09:15:00"
    "What happens on the edge between dynamic and static types in graduated typed language: Boxing and unboxing (dynamic are objects)?"
    "Anonymous")
 #s(slido-question
    "2018-09-15T19:12:00"
    "Is this a good question?"
    "Anonymous")
 #s(slido-question
    "2018-09-15T19:13:00"
    "How many questions will we ask to test this out?"
    "Anonymous")
 #s(slido-question
    "2018-09-15T19:16:00"
    "How much wood could a woodchuck chuck if a woodchuck could chuck wood"
    "Anonymous")
 #s(slido-question
    "2018-09-15T19:16:00"
    "Is it easy to use a phone to ask questions?"
    "Anonymous")
 #s(slido-question "2018-09-15T19:16:00" "你們會不會講中文" "Anonymous"))
