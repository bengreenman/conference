;; (struct channel [slack-id url author* doi] #:prefab)
;; (struct author [name url] #:prefab)
(#s(channel
    "icfp-build-systems-a-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-build-systems-a-la-carte"
    (#s(author
        "Andrey Mokhov"
        "https://icfp18.sigplan.org/profile/andreymokhov")
     #s(author
        "Neil Mitchell"
        "https://icfp18.sigplan.org/profile/neilmitchell")
     #s(author
        "Simon Peyton Jones"
        "https://icfp18.sigplan.org/profile/simonpaytonjones"))
    "http://doi.org/10.1145/3236774")
 #s(channel
    "icfp-keep-your-lazine"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-keep-your-laziness-in-check"
    (#s(author
        "Kenneth Foner"
        "https://icfp18.sigplan.org/profile/kennethfoner")
     #s(author
        "Hengchu Zhang"
        "https://icfp18.sigplan.org/profile/hengchuzhang")
     #s(author
        "Leonidas Lampropoulos"
        "https://icfp18.sigplan.org/profile/leonidaslampropoulos"))
    "http://doi.org/10.1145/3236797")
 #s(channel
    "icfp-merlin-a-languag"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-experience-report-merlin-a-language-server-for-ocaml"
    (#s(author
        "Frédéric Bour"
        "https://icfp18.sigplan.org/profile/fredericbour")
     #s(author "Thomas Réfis" "https://icfp18.sigplan.org/profile/thomasrefis")
     #s(author
        "Gabriel Scherer"
        "https://icfp18.sigplan.org/profile/gabrielscherer"))
    "http://doi.org/10.1145/3236798")
 #s(channel
    "icfp-functional-progr"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-functional-programming-for-compiling-and-decompiling-computer-aided-design"
    (#s(author
        "Chandrakana Nandi"
        "https://icfp18.sigplan.org/profile/chandrakananandi")
     #s(author
        "James R. Wilcox"
        "https://icfp18.sigplan.org/profile/jamesrwilcox")
     #s(author "Taylor Blau" "https://icfp18.sigplan.org/profile/taylorblau")
     #s(author "Dan Grossman" "https://icfp18.sigplan.org/profile/dangrossman")
     #s(author
        "Zachary Tatlock"
        "https://icfp18.sigplan.org/profile/zacharytatlock"))
    "http://doi.org/10.1145/3236794")
 #s(channel
    "icfp-prototyping-a-fu"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-prototyping-a-functional-language-using-higher-order-logic-programming-a-functional-pearl-on-learning-the-ways-of-prolog-makam"
    (#s(author
        "Antonis Stampoulis"
        "https://icfp18.sigplan.org/profile/antonisstampoulis")
     #s(author
        "Adam Chlipala"
        "https://icfp18.sigplan.org/profile/adamchlipala"))
    "http://doi.org/10.1145/3236788")
 #s(channel
    "icfp-a-type-and-scope"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-a-type-and-scope-safe-universe-of-syntaxes-with-binding-their-semantics-and-proofs"
    (#s(author
        "Guillaume Allais"
        "https://icfp18.sigplan.org/profile/guillaumeallais")
     #s(author "Robert Atkey" "https://icfp18.sigplan.org/profile/robertatkey")
     #s(author
        "James Chapman"
        "https://icfp18.sigplan.org/profile/jameschapman")
     #s(author
        "Conor McBride"
        "https://icfp18.sigplan.org/profile/conormcbride")
     #s(author
        "James McKinna"
        "https://icfp18.sigplan.org/profile/jamesmckinna1"))
    "http://doi.org/10.1145/3236785")
 #s(channel
    "icfp-reasonably-progr"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-reasonably-programmable-literal-notation"
    (#s(author "Cyrus Omar" "https://icfp18.sigplan.org/profile/cyrusomar")
     #s(author
        "Jonathan Aldrich"
        "https://icfp18.sigplan.org/profile/jonathanaldrich"))
    "http://doi.org/10.1145/3236801")
 #s(channel
    "icfp-refunctionalizat"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-refunctionalization-of-abstract-abstract-machines"
    (#s(author "Guannan Wei" "https://icfp18.sigplan.org/profile/guannanwei")
     #s(author "James Decker" "https://icfp18.sigplan.org/profile/jamesdecker")
     #s(author "Tiark Rompf" "https://icfp18.sigplan.org/profile/tiarkrompf"))
    "http://doi.org/10.1145/3236800")
 #s(channel
    "icfp-capturing-the-fu"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-capturing-the-future-by-replaying-the-past"
    (#s(author "James Koppel" "https://icfp18.sigplan.org/profile/jameskoppel")
     #s(author
        "Gabriel Scherer"
        "https://icfp18.sigplan.org/profile/gabrielscherer")
     #s(author
        "Armando Solar-Lezama"
        "https://icfp18.sigplan.org/profile/armandosolarlezama"))
    "http://doi.org/10.1145/3236771")
 #s(channel
    "icfp-handling-delimit"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-handling-delimited-continuations-with-dependent-types"
    (#s(author "Youyou Cong" "https://icfp18.sigplan.org/profile/youyoucong")
     #s(author
        "Kenichi Asai"
        "https://icfp18.sigplan.org/profile/kenichiasai"))
    "http://doi.org/10.1145/3236764")
 #s(channel
    "icfp-versatile-event-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-versatile-event-correlation-with-algebraic-effects"
    (#s(author
        "Oliver Bračevac"
        "https://icfp18.sigplan.org/profile/oliverbracevac")
     #s(author "Nada Amin" "https://icfp18.sigplan.org/profile/nadaamin")
     #s(author
        "Guido Salvaneschi"
        "https://icfp18.sigplan.org/profile/guidosalvaneschi")
     #s(author
        "Sebastian Erdweg"
        "https://icfp18.sigplan.org/profile/sebastianerdweg")
     #s(author
        "Patrick Eugster"
        "https://icfp18.sigplan.org/profile/patrickeugster")
     #s(author "Mira Mezini" "https://icfp18.sigplan.org/profile/miramezini"))
    "http://doi.org/10.1145/3236762")
 #s(channel
    "icfp-the-simple-essen"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-the-simple-essence-of-automatic-differentiation-differentiable-functional-programming-made-easy-"
    (#s(author
        "Conal Elliott"
        "https://icfp18.sigplan.org/profile/conalelliott"))
    "http://doi.org/10.1145/3236765")
 #s(channel
    "icfp-functional-progr"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-functional-programming-for-modular-bayesian-inference"
    (#s(author "Adam Ścibior" "https://icfp18.sigplan.org/profile/adamscibior")
     #s(author "Ohad Kammar" "https://icfp18.sigplan.org/profile/ohadkammar")
     #s(author
        "Zoubin Ghahramani"
        "https://icfp18.sigplan.org/profile/zoubinghahramani"))
    "http://doi.org/10.1145/3236778")
 #s(channel
    "icfp-contextual-equiv"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-contextual-equivalence-for-a-probabilistic-language-with-continuous-random-variables-and-recursion"
    (#s(author
        "Mitchell Wand"
        "https://icfp18.sigplan.org/profile/mitchellwand")
     #s(author
        "Ryan Culpepper"
        "https://icfp18.sigplan.org/profile/ryanculpepper")
     #s(author
        "Theophilos Giannakopoulos"
        "https://icfp18.sigplan.org/profile/theophilosgiannakopoulos")
     #s(author "Andrew Cobb" "https://icfp18.sigplan.org/profile/andrewcobb"))
    "http://doi.org/10.1145/3236782")
 #s(channel
    "icfp-teaching-how-to-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-experience-report-teaching-how-to-program-using-automated-assessment-and-functional-glossy-games"
    (#s(author
        "José Bacelar Almeira"
        "https://icfp18.sigplan.org/profile/josebacelaralmeira")
     #s(author "Alcino Cunha" "https://icfp18.sigplan.org/profile/alcinocunha")
     #s(author "Nuno Macedo" "https://icfp18.sigplan.org/profile/nunomacedo")
     #s(author "Hugo Pacheco" "https://icfp18.sigplan.org/profile/hugopacheco")
     #s(author
        "José Proença"
        "https://icfp18.sigplan.org/profile/joseproenca"))
    "http://doi.org/10.1145/3236777")
 #s(channel
    "icfp-competitive-para"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-competitive-parallelism-getting-your-priorities-right"
    (#s(author
        "Stefan K. Muller"
        "https://icfp18.sigplan.org/profile/stefankmuller")
     #s(author "Umut Acar" "https://icfp18.sigplan.org/profile/umutacar")
     #s(author
        "Robert Harper"
        "https://icfp18.sigplan.org/profile/robertharper1"))
    "http://doi.org/10.1145/3236790")
 #s(channel
    "icfp-static-interpret"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-static-interpretation-of-higher-order-modules-in-futhark"
    (#s(author
        "Martin Elsman"
        "https://icfp18.sigplan.org/profile/martinelsman")
     #s(author
        "Troels Henriksen"
        "https://icfp18.sigplan.org/profile/troelshenriksen")
     #s(author
        "Danil Annenkov"
        "https://icfp18.sigplan.org/profile/danilannenkov")
     #s(author
        "Cosmin Oancea"
        "https://icfp18.sigplan.org/profile/cosminoancea"))
    "http://doi.org/10.1145/3236792")
 #s(channel
    "icfp-finitary-polymor"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-finitary-polymorphism-for-optimizing-type-directed-compilation"
    (#s(author
        "Atsushi Ohori"
        "https://icfp18.sigplan.org/profile/atsushiohori")
     #s(author
        "Katsuhiro Ueno"
        "https://icfp18.sigplan.org/profile/katsuhiroueno")
     #s(author
        "Hisayuki Mima"
        "https://icfp18.sigplan.org/profile/hisayukimima"))
    "http://doi.org/10.1145/3236776")
 #s(channel
    "icfp-fault-tolerant-f"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-fault-tolerant-functional-reactive-programming"
    (#s(author "Ivan Perez" "https://icfp18.sigplan.org/profile/ivanperez"))
    "http://doi.org/10.1145/3236791")
 #s(channel
    "icfp-mosel-a-general-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-mosel-a-general-extensible-modal-framework-for-interactive-proofs-in-separation-logic"
    (#s(author
        "Robbert Krebbers"
        "https://icfp18.sigplan.org/profile/robbertkrebbers")
     #s(author
        "Jacques-Henri Jourdan"
        "https://icfp18.sigplan.org/profile/jacqueshenrijourdan")
     #s(author "Ralf Jung" "https://icfp18.sigplan.org/profile/ralfjung")
     #s(author
        "Joseph Tassarotti"
        "https://icfp18.sigplan.org/profile/josephtassarotti")
     #s(author
        "Jan-Oliver Kaiser"
        "https://icfp18.sigplan.org/profile/janoliverkaiser")
     #s(author "Amin Timany" "https://icfp18.sigplan.org/profile/amintimany")
     #s(author
        "Arthur Charguéraud"
        "https://icfp18.sigplan.org/profile/arthurchargueraud")
     #s(author
        "Derek Dreyer"
        "https://icfp18.sigplan.org/profile/derekdreyer"))
    "http://doi.org/10.1145/3236772")
 #s(channel
    "icfp-mtac2-typed-tact"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-mtac2-typed-tactics-for-backward-reasoning-in-coq"
    (#s(author
        "Jan-Oliver Kaiser"
        "https://icfp18.sigplan.org/profile/janoliverkaiser")
     #s(author "Beta Ziliani" "https://icfp18.sigplan.org/profile/betaziliani")
     #s(author
        "Robbert Krebbers"
        "https://icfp18.sigplan.org/profile/robbertkrebbers")
     #s(author
        "Yann Régis-Gianas"
        "https://icfp18.sigplan.org/profile/yannregisgianas")
     #s(author
        "Derek Dreyer"
        "https://icfp18.sigplan.org/profile/derekdreyer"))
    "http://doi.org/10.1145/3236773")
 #s(channel
    "icfp-compositional-so"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-compositional-soundness-proofs-of-abstract-interpreters"
    (#s(author "Sven Keidel" "https://icfp18.sigplan.org/profile/svenkeidel")
     #s(author
        "Casper Bach Poulsen"
        "https://icfp18.sigplan.org/profile/casperbachpoulsen")
     #s(author
        "Sebastian Erdweg"
        "https://icfp18.sigplan.org/profile/sebastianerdweg"))
    "http://doi.org/10.1145/3236767")
 #s(channel
    "icfp-equivalences-for"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-equivalences-for-free-univalent-parametricity-for-effective-transport"
    (#s(author
        "Nicolas Tabareau"
        "https://icfp18.sigplan.org/profile/nicolastabareau")
     #s(author "Éric Tanter" "https://icfp18.sigplan.org/profile/etanter")
     #s(author
        "Matthieu Sozeau"
        "https://icfp18.sigplan.org/profile/matthieusozeau"))
    "http://doi.org/10.1145/3236787")
 #s(channel
    "icfp-what-you-needa-k"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-what-you-needa-know-about-yoneda"
    (#s(author
        "Guillaume Boisseau"
        "https://icfp18.sigplan.org/profile/guillaumeboisseau")
     #s(author
        "Jeremy Gibbons"
        "https://icfp18.sigplan.org/profile/jeremygibbons"))
    "http://doi.org/10.1145/3236779")
 #s(channel
    "icfp-incremental-rela"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-incremental-relational-lenses"
    (#s(author "Rudi Horn" "https://icfp18.sigplan.org/profile/rudihorn")
     #s(author "Roly Perera" "https://icfp18.sigplan.org/profile/rolyperera")
     #s(author
        "James Cheney"
        "https://icfp18.sigplan.org/profile/jamescheney"))
    "http://doi.org/10.1145/3236769")
 #s(channel
    "icfp-synthesizing-quo"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-synthesizing-quotient-lenses"
    (#s(author
        "Solomon Maina"
        "https://icfp18.sigplan.org/profile/solomonmaina")
     #s(author
        "Anders Miltner"
        "https://icfp18.sigplan.org/profile/andersmiltner1")
     #s(author
        "Kathleen Fisher"
        "https://icfp18.sigplan.org/profile/kathleenfisher")
     #s(author
        "Benjamin C. Pierce"
        "https://icfp18.sigplan.org/profile/benjamincpierce")
     #s(author "Dave Walker" "https://icfp18.sigplan.org/profile/davewalker")
     #s(author
        "Steve Zdancewic"
        "https://icfp18.sigplan.org/profile/stevezdancewic"))
    "http://doi.org/10.1145/3236775")
 #s(channel
    "icfp-generic-deriving"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-generic-deriving-of-generic-traversals"
    (#s(author "Csongor Kiss" "https://icfp18.sigplan.org/profile/csongorkiss")
     #s(author
        "Matthew Pickering"
        "https://icfp18.sigplan.org/profile/matthewpickering")
     #s(author "Nicolas Wu" "https://icfp18.sigplan.org/profile/nicolaswu"))
    "http://doi.org/10.1145/3236780")
 #s(channel
    "icfp-partially-static"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-partially-static-data-as-free-extension-of-algebras"
    (#s(author
        "Jeremy Yallop"
        "https://icfp18.sigplan.org/profile/jeremyyallop")
     #s(author
        "Tamara von Glehn"
        "https://icfp18.sigplan.org/profile/tamaravonglehn")
     #s(author "Ohad Kammar" "https://icfp18.sigplan.org/profile/ohadkammar"))
    "http://doi.org/10.1145/3236795")
 #s(channel
    "icfp-relational-algeb"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-relational-algebra-by-way-of-adjunctions"
    (#s(author
        "Jeremy Gibbons"
        "https://icfp18.sigplan.org/profile/jeremygibbons")
     #s(author
        "Fritz Henglein"
        "https://icfp18.sigplan.org/profile/fritzhenglein")
     #s(author "Ralf Hinze" "https://icfp18.sigplan.org/profile/ralfhinze")
     #s(author "Nicolas Wu" "https://icfp18.sigplan.org/profile/nicolaswu"))
    "http://doi.org/10.1145/3236781")
 #s(channel
    "icfp-strict-and-lazy-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-strict-and-lazy-semantics-of-effects"
    (#s(author
        "Andrew Hirsch"
        "https://icfp18.sigplan.org/profile/andrewhirsch")
     #s(author "Ross Tate" "https://icfp18.sigplan.org/profile/rosstate"))
    "http://doi.org/10.1145/3236783")
 #s(channel
    "icfp-what-s-the-diffe"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-what-s-the-difference-a-functional-pearl-on-subtracting-bijections"
    (#s(author "Brent Yorgey" "https://icfp18.sigplan.org/profile/brentyorgey")
     #s(author
        "Kenneth Foner"
        "https://icfp18.sigplan.org/profile/kennethfoner"))
    "http://doi.org/10.1145/3236796")
 #s(channel
    "icfp-a-spectrum-of-so"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-a-spectrum-of-soundness-and-performance"
    (#s(author "Ben Greenman" "https://icfp18.sigplan.org/profile/bengreenman")
     #s(author
        "Matthias Felleisen"
        "https://icfp18.sigplan.org/profile/matthiasfelleisen"))
    "http://doi.org/10.1145/3236766")
 #s(channel
    "icfp-casts-and-costs-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-casts-and-costs-harmonizing-safety-and-performance-in-gradual-typing"
    (#s(author
        "John Peter Campora"
        "https://icfp18.sigplan.org/profile/johnpetercamporaiii1")
     #s(author "Sheng Chen" "https://icfp18.sigplan.org/profile/shengchen1")
     #s(author
        "Eric Walkingshaw"
        "https://icfp18.sigplan.org/profile/ericwalkingshaw"))
    "http://doi.org/10.1145/3236793")
 #s(channel
    "icfp-graduality-from-"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-graduality-from-embedding-projection-pairs"
    (#s(author "Max S. New" "https://icfp18.sigplan.org/profile/maxsnew")
     #s(author "Amal Ahmed" "https://icfp18.sigplan.org/profile/amalahmed"))
    "http://doi.org/10.1145/3236768")
 #s(channel
    "icfp-ready-set-verify"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-ready-set-verify-applying-hs-to-coq-to-real-world-haskell-code-experience-report-"
    (#s(author
        "Joachim Breitner"
        "https://icfp18.sigplan.org/profile/joachimbreitner")
     #s(author
        "Antal Spector-Zabusky"
        "https://icfp18.sigplan.org/profile/antalspectorzabusky")
     #s(author "Yao Li" "https://icfp18.sigplan.org/profile/yaoli")
     #s(author
        "Christine Rizkallah"
        "https://icfp18.sigplan.org/profile/christinerizkallah")
     #s(author "John Wiegley" "https://icfp18.sigplan.org/profile/johnwiegley")
     #s(author
        "Stephanie Weirich"
        "https://icfp18.sigplan.org/profile/stephanieweirich"))
    "http://doi.org/10.1145/3236784")
 #s(channel
    "icfp-parallel-complex"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-parallel-complexity-analysis-with-temporal-session-types"
    (#s(author "Ankush Das" "https://icfp18.sigplan.org/profile/ankushdas")
     #s(author "Jan Hoffmann" "https://icfp18.sigplan.org/profile/janhoffmann")
     #s(author
        "Frank Pfenning"
        "https://icfp18.sigplan.org/profile/frankpfenning"))
    "http://doi.org/10.1145/3236786")
 #s(channel
    "icfp-parametric-polym"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-parametric-polymorphism-and-operational-improvement"
    (#s(author
        "Jennifer Hackett"
        "https://icfp18.sigplan.org/profile/jenniferhackett")
     #s(author
        "Graham Hutton"
        "https://icfp18.sigplan.org/profile/grahamhutton"))
    "http://doi.org/10.1145/3236763")
 #s(channel
    "icfp-tight-typings-an"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-tight-typings-and-split-bounds"
    (#s(author
        "Beniamino Accattoli"
        "https://icfp18.sigplan.org/profile/beniaminoaccattoli")
     #s(author
        "Stéphane Graham-Lengrand"
        "https://icfp18.sigplan.org/profile/stephanegrahamlengrand")
     #s(author
        "Delia Kesner"
        "https://icfp18.sigplan.org/profile/deliakesner"))
    "http://doi.org/10.1145/3236789")
 #s(channel
    "icfp-elaborating-depe"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-elaborating-dependent-co-pattern-matching"
    (#s(author "Jesper Cockx" "https://icfp18.sigplan.org/profile/jespercockx")
     #s(author
        "Andreas Abel"
        "https://icfp18.sigplan.org/profile/andreasabel"))
    "http://doi.org/10.1145/3236770")
 #s(channel
    "icfp-generic-zero-cos"
    "https://icfp18.sigplan.org/event/icfp-2018-papers-generic-zero-cost-reuse-for-dependent-types"
    (#s(author "Larry Diehl" "https://icfp18.sigplan.org/profile/larrydiehl")
     #s(author "Denis Firsov" "https://icfp18.sigplan.org/profile/denisfirsov")
     #s(author "Aaron Stump" "https://icfp18.sigplan.org/profile/aaronstump"))
    "http://doi.org/10.1145/3236799"))
