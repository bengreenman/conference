ARCHIVE="slack-archive"
for chn in casts-and-costs fault-tolerant finitary-polymorphism keep-your-laziness merlin modular-bayesian parametric-poly partially-static refunctionalization strict-and-lazy the-simple-essence tight-typings; do
  echo "${chn}"
  emacs ARCHIVE/${chn}.txt;
  read;
done;
