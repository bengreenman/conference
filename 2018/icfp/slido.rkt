#lang racket/base

;; sli.do question parser
;; - keep a database of all archived sli.do questions
;; - parse questions + timestamps from hand-downloaded HTML,
;;   add questions to the database,
;;   ignore replies/upvotes to questions? or do something reasonable
;; - keep the conference program, or a modified program
;; - map conf. sessions to slack channels
;; - query / print the sli.do questions that match a session + channel

;; ICFP url : https://admin.sli.do/event/0mjm6dz9/questions
;; mflatt@cs.utah.edu
;; whatisyourquest

(require racket/contract)
(provide
  channel->time-bounds
  timestamp-within

  (contract-out
    [init-db (->* [] [(or/c #f path-string?)] slido-db?)]
    [write-db (-> slido-db? void?)]
    [add-from-html (-> slido-db? file-exists? slido-db?)]
    [slack-channel-id? (-> any/c boolean?)]
    [find-question* (-> slido-db? (or/c (cons/c string? string?) slack-channel-id?) (listof slido-question?))]
    [print-question* (->* [(listof slido-question?)] [any/c] void?)]
))

(require
  (for-syntax racket/base syntax/parse)
  (only-in gregor
    current-clock current-timezone iso8601->datetime datetime->iso8601
    parse-time parse-date parse-datetime today datetime ->year ->month ->day ->hours
    ->minutes datetime<=? date->iso8601)
  html-parsing
  sxml
  (only-in net/url string->url call/input-url get-impure-port)
  (only-in racket/string string-trim string-replace string-split)
  (only-in racket/runtime-path define-runtime-path)
  (only-in racket/pretty pretty-write)
  (only-in racket/file file->value))

(module+ test
  (require rackunit))

(current-timezone "America/Chicago") ;; St. Louis timezone

;; =============================================================================

(define-runtime-path slido.rktd "./src/slido.rktd")
(define-runtime-path sample-questions.html "./src/slido-questions.html")
(define-runtime-path icfp-program.rktd "./src/icfp-program.rktd")
(define-runtime-path icfp-timeline.rktd "./src/icfp-timeline.rktd")

(struct channel [slack-id url author* doi] #:prefab)
(struct author [name url] #:prefab)

(define the-channel*
  (if (not (file-exists? icfp-program.rktd))
    (raise-arguments-error 'slido "file-exists?" "icfp file" icfp-program.rktd)
    (file->value icfp-program.rktd)))

(define the-timeline*
  (if (not (file-exists? icfp-timeline.rktd))
    #f
    (file->value icfp-timeline.rktd)))

(define-syntax (keep-src-as stx)
  (syntax-parse stx
    [(_ ?id:id ?expr)
     #'(begin
         (define ?id '?expr)
         ?expr)]))

(keep-src-as slido-question-stx
  (struct slido-question [timestamp text author] #:prefab))

(struct slido-db [file question*] #:prefab)

(define (init-db [pre-ps #f])
  (define ps (or pre-ps slido.rktd))
  (define q*
    (if (file-exists? ps)
      (file->value ps)
      '()))
  (slido-db ps q*))

(define (write-db db)
  (define dst (slido-db-file db))
  (define q* (slido-db-question* db))
  ;; TODO check if file exists, ask before overwriting?
  (with-output-to-file dst #:exists 'replace
    (lambda ()
      (void
        (display ";; ")
        (writeln (values slido-question-stx)))
      (pretty-write q*))))

(define (add-from-html db html)
  (define q* (html->question* html))
  (define db+ (add-question* db q*))
  (write-db db+)
  db+)

(define (html->question* html)
  (define xxp (call-with-input-file html html->xexp))
  (map xexp->q (xexp->q* xxp)))

(define xexp->q*
  (sxpath "//div[contains(@class, 'question ')]"))

(define (xexp->q xxp)
  (define timestamp (q->timestamp xxp))
  (define txt (q->text xxp))
  (define a (q->author xxp))
  (and timestamp a txt (slido-question timestamp txt a)))

(define ((sxpath/trim pat) xxp)
  (define t* ((sxpath pat) xxp))
  (apply string-append (map string-trim t*)))

(define (q->timestamp xxp)
  (define str ((sxpath/trim "//span[contains(@ng-hide, 'question.is_public')]/text()") xxp))
  (parse-timestamp str))

(define (parse-timestamp str)
  (define str* (string-split str ","))
  (define d
    (let ((d-str (car str*)))
      (if (string=? d-str "today")
        (today)
        (parse-date (string-append "2018 " d-str) "y d LLL"))))
  (define t (parse-time (string-replace (cadr str*) " " "0") "HH:mma"))
  (define dt (datetime (->year d)
                       (->month d)
                       (->day d)
                       (->hours t)
                       (->minutes t)))
  (datetime->iso8601 dt))

(define q->text
  (sxpath/trim "//div[contains(@class, 'question__text')]/text()"))

(define q->author
  (sxpath/trim "//div[contains(@class, 'question__author')]/text()"))

(module+ test
  (define q-xexp (html->xexp "<!-- ngRepeat: question in $ctrl.mobileQuestions | limitTo:$ctrl.config.limits.public track by question.event_question_id --><div class=\"question bg-white ng-scope\" ng-repeat=\"question in $ctrl.mobileQuestions | limitTo:$ctrl.config.limits.public track by question.event_question_id\" ng-class=\"{ 'is-highlighted': question.is_highlighted &amp;&amp; $ctrl.config.filter.public != 'archived' &amp;&amp; !$ctrl.isPublicFilterTop(), 'is-pinned': question.is_bookmarked }\" ng-show=\"!(question.is_highlighted &amp;&amp; $ctrl.isPublicFilterTop())\" ng-click=\"$event.stopPropagation();$ctrl.setQuestion(question)\"> <div class=\"question__header flex items-center l-mb1\"> <user-avatar image=\"question.author.attrs.avatar\" name=\"question.author.name\" revert=\"question.is_highlighted\" class=\"ng-isolate-scope\"><div ng-class=\"{'revert': $ctrl.revert}\" class=\"user-avatar  l-mr1\" ng-switch=\"$ctrl.getType()\"> <div class=\"flex flex-center flex-x-center\"> <!-- ngSwitchWhen: initials --> <!-- ngSwitchWhen: icon --><div ng-switch-when=\"icon\" class=\"ng-scope\"> <svg-icon icon=\"md-person\" class=\"ui-f-white icon-size14 block ng-isolate-scope\"><svg viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z\"></path></svg></svg-icon> </div><!-- end ngSwitchWhen: --> <!-- ngSwitchWhen: image --> </div> </div> </user-avatar> <div class=\"overflow-hidden\"> <div class=\"question__author truncate ng-binding\">Anonymous</div> <div class=\"question__when\"> <span class=\"question__like\"> <span class=\"inline-block va-middle ng-binding\">0 </span> <svg-icon icon=\"like-o\" class=\"ui-f-grey-2 icon-size12 ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>like-o</title><path d=\"M22.391 13.397c0.096 0.338 0.145 0.675 0.145 1.013 0 0.723-0.193 1.447-0.579 2.073 0.048 0.193 0.048 0.434 0.048 0.627 0 0.916-0.289 1.832-0.868 2.556 0.048 2.748-1.832 4.34-4.484 4.34h-1.881c-2.025 0-3.954-0.627-5.834-1.254-0.434-0.145-1.591-0.579-1.977-0.579h-4.147c-1.061 0-1.881-0.82-1.881-1.881v-9.21c0-1.013 0.82-1.832 1.881-1.832h3.906c0.579-0.386 1.543-1.688 1.977-2.266 0.53-0.627 1.013-1.254 1.543-1.832 0.868-0.916 0.434-3.182 1.881-4.629 0.338-0.289 0.771-0.53 1.302-0.53 1.495 0 2.941 0.53 3.616 1.929 0.434 0.916 0.53 1.736 0.53 2.7 0 1.013-0.289 1.881-0.723 2.748h2.556c1.977 0 3.665 1.688 3.665 3.713 0 0.82-0.241 1.639-0.675 2.314zM19.402 9.251h-5.063c0-1.688 1.35-2.941 1.35-4.629s-0.289-2.748-2.266-2.748c-0.964 0.916-0.482 3.134-1.881 4.581-0.386 0.434-0.723 0.868-1.109 1.302-0.627 0.868-2.363 3.327-3.472 3.327h-0.482v9.21h0.482c0.771 0 2.122 0.53 2.893 0.82 1.591 0.53 3.231 1.061 4.918 1.061h1.736c1.639 0 2.797-0.675 2.797-2.411 0-0.289-0.048-0.579-0.096-0.82 0.627-0.338 0.964-1.157 0.964-1.832 0-0.338-0.096-0.675-0.289-0.964 0.53-0.482 0.771-1.061 0.771-1.736 0-0.482-0.193-1.157-0.482-1.495 0.675 0 1.061-1.302 1.061-1.832 0-0.964-0.868-1.832-1.832-1.832z\"></path></svg></svg-icon> </span> <!-- ngIf: $ctrl.event.attrs.questions.enable_downvote --> <span class=\"inline-block va-middle\"> <span class=\"question__dot\">•</span> <span ng-hide=\"question.is_public\" class=\"ng-binding ng-hide\"> today, 7:16pm </span> <span ng-show=\"question.is_public\" class=\"ng-binding\"> today, 7:16pm </span> </span> <span class=\"inline-block va-middle ng-hide\" ng-show=\"question.is_answered &amp;&amp; !question.is_public\"> <span class=\"question__dot\">•</span> <span class=\"c-red\">(unapproved)</span> </span> </div> </div> </div> <div class=\"question__text ng-binding\"> 你們會不會講中文 </div> <div class=\"question__reply pointer ng-hide\" ng-show=\"question.attrs.replies_count > 0\"> <span> <svg-icon icon=\"reply\" class=\"icon-size12 ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>reply</title><path d=\"M24 15.445c0 1.5-0.563 3.516-1.688 6.047-0.047 0.047-0.094 0.141-0.141 0.328-0.094 0.141-0.141 0.281-0.188 0.375-0.047 0.141-0.141 0.234-0.188 0.328-0.094 0.141-0.234 0.188-0.375 0.188s-0.234-0.047-0.328-0.094c-0.047-0.094-0.094-0.234-0.094-0.375 0-0.047 0-0.188 0.047-0.328 0-0.141 0-0.281 0-0.328 0.047-0.609 0.094-1.172 0.094-1.641 0-0.891-0.094-1.688-0.234-2.438-0.141-0.703-0.375-1.313-0.656-1.828s-0.656-0.984-1.078-1.359c-0.422-0.375-0.891-0.703-1.406-0.938s-1.078-0.422-1.781-0.563c-0.703-0.141-1.359-0.234-2.063-0.281s-1.453-0.094-2.344-0.094h-3v3.422c0 0.234-0.094 0.422-0.281 0.609-0.141 0.188-0.328 0.234-0.563 0.234s-0.469-0.047-0.609-0.234l-6.891-6.844c-0.141-0.188-0.234-0.375-0.234-0.609s0.094-0.422 0.234-0.609l6.891-6.844c0.141-0.188 0.375-0.281 0.609-0.281s0.422 0.094 0.563 0.281c0.188 0.141 0.281 0.375 0.281 0.609v3.422h3c6.375 0 10.266 1.781 11.719 5.391 0.469 1.172 0.703 2.672 0.703 4.453z\"></path></svg></svg-icon> </span> <span class=\"ui-c-grey-1 ng-binding\"> replies</span> </div> <div class=\"question__actions\"> <action-state item=\"question\" class=\"ng-isolate-scope\"><span ng-show=\"$ctrl.item._state == 'pending'\" class=\"action-checker-icon action-checker-icon__spinner spinner ng-hide\"> </span> <span class=\"action-checker-icon action-checker-icon__warning ng-hide hint--bottom\" ng-show=\"$ctrl.item._state == 'failed'\" ng-class=\"$ctrl.hintPosition\" data-hint=\"Action failed. Please try refreshing your browser.\"> <svg-icon icon=\"md-error-outline\" class=\"icon-size16 ui-f-red ng-isolate-scope\"><svg viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M11 15h2v2h-2zm0-8h2v6h-2zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z\"></path></svg></svg-icon> </span> </action-state> <!-- Live and Pinned --> <span ng-hide=\"question.is_answered\" class=\"ng-hide\"> <button class=\"btn btn-clear l-mr05 ng-hide\" ng-show=\"question.is_highlighted\" ng-click=\"$event.stopPropagation(); $ctrl.EventQuestionsActions.restore(question)\"> <svg-icon icon=\"unhighlight\" class=\"question__actions-button ui-f-white ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>unhighlight</title><path d=\"M12 24c6.628 0 12-5.372 12-12s-5.372-12-12-12-12 5.372-12 12 5.372 12 12 12zM12 0.716c6.234 0 11.284 5.050 11.284 11.284s-5.050 11.284-11.284 11.284-11.284-5.050-11.284-11.284 5.050-11.284 11.284-11.284v0z\"></path><path d=\"M15.469 9.469l1.097 1.094-4.659 4.656-4.659-4.656 1.097-1.094 3.563 3.553z\"></path></svg></svg-icon> </button> <button class=\"btn btn-clear l-mr05\" ng-show=\"!question.is_highlighted\" ng-click=\"$event.stopPropagation(); $ctrl.EventQuestionsActions.highlight(question)\"> <svg-icon icon=\"highlight\" class=\"question__actions-button ui-f-blue-1 ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>highlight</title><path d=\"M12 0c-6.628 0-12 5.372-12 12s5.372 12 12 12 12-5.372 12-12-5.372-12-12-12zM12 23.284c-6.234 0-11.284-5.050-11.284-11.284s5.050-11.284 11.284-11.284 11.284 5.050 11.284 11.284-5.050 11.284-11.284 11.284v0z\"></path><path d=\"M8.781 16.704l-0.989-0.989 4.208-4.209 4.209 4.209-0.99 0.989-3.219-3.213-3.219 3.213zM8.781 12.495l-0.989-0.989 4.208-4.209 4.209 4.209-0.99 0.989-3.219-3.213z\"></path></svg></svg-icon> </button> <button class=\"btn btn-clear\" ng-click=\"$event.stopPropagation(); $ctrl.EventQuestionsActions.remove(question)\"> <svg-icon icon=\"archive\" class=\"ui-f-grey-5 question__actions-button ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>archive</title><path d=\"M12 0c-6.628 0-12 5.372-12 12s5.372 12 12 12 12-5.372 12-12-5.372-12-12-12zM12 23.284c-6.234 0-11.284-5.050-11.284-11.284s5.050-11.284 11.284-11.284 11.284 5.050 11.284 11.284-5.050 11.284-11.284 11.284z\"></path><path d=\"M16.763 7.281h-9.524c-0.376 0-0.681 0.305-0.681 0.681v1.359h10.883v-1.359c0-0.001 0-0.001 0-0.002 0-0.375-0.304-0.679-0.678-0.679z\"></path><path d=\"M7.238 16.125c0 0.376 0.305 0.681 0.681 0.681h8.162c0.376-0 0.681-0.305 0.681-0.681v-6.122h-9.524v6.122zM10.98 11.363h2.040c0.376 0 0.681 0.305 0.681 0.681s-0.305 0.681-0.681 0.681h-2.040c-0.376 0-0.681-0.305-0.681-0.681s0.305-0.681 0.681-0.681z\"></path></svg></svg-icon> </button> </span> <!-- Archived --> <span ng-show=\"question.is_answered\"> <button class=\"btn btn-clear l-mr05\" ng-click=\"$ctrl.bookmark($event, question)\"> <span ng-show=\"question.is_bookmarked\" class=\"ng-hide\"> <svg-icon icon=\"star\" class=\"question__actions-button-sm ui-f-orange-2 ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>star</title><path d=\"M24.006 9.427c0 0.202-0.151 0.454-0.404 0.706l-5.196 5.095 1.211 7.214c0 0.050 0 0.151 0 0.252 0 0.202-0.050 0.404-0.151 0.504-0.101 0.151-0.202 0.252-0.404 0.252s-0.404-0.101-0.605-0.202l-6.457-3.38-6.457 3.38c-0.202 0.101-0.404 0.202-0.605 0.202s-0.353-0.101-0.454-0.252c-0.101-0.101-0.151-0.303-0.151-0.504 0-0.050 0-0.151 0.050-0.252l1.211-7.214-5.246-5.095c-0.202-0.303-0.353-0.504-0.353-0.706 0-0.353 0.303-0.605 0.807-0.656l7.264-1.059 3.228-6.558c0.202-0.404 0.404-0.605 0.706-0.605s0.504 0.202 0.706 0.605l3.228 6.558 7.264 1.059c0.504 0.050 0.807 0.303 0.807 0.656z\"></path></svg></svg-icon> </span> <span ng-show=\"!question.is_bookmarked\"> <svg-icon icon=\"star-o\" class=\"question__actions-button-sm ui-f-grey-10 ui-f-h-grey-2 ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>star-o</title><path d=\"M16.389 14.573l4.439-4.288-6.104-0.908-2.724-5.498-2.724 5.498-6.104 0.908 4.439 4.288-1.059 6.053 5.448-2.875 5.448 2.875zM24.006 9.427c0 0.202-0.151 0.454-0.404 0.706l-5.196 5.095 1.211 7.214c0 0.050 0 0.151 0 0.252 0 0.504-0.151 0.757-0.555 0.757-0.202 0-0.404-0.101-0.605-0.202l-6.457-3.38-6.457 3.38c-0.202 0.101-0.404 0.202-0.605 0.202s-0.353-0.101-0.454-0.252c-0.101-0.101-0.151-0.303-0.151-0.504 0-0.050 0-0.151 0.050-0.252l1.211-7.214-5.246-5.095c-0.202-0.303-0.353-0.504-0.353-0.706 0-0.353 0.303-0.605 0.807-0.656l7.264-1.059 3.228-6.558c0.202-0.404 0.404-0.605 0.706-0.605s0.504 0.202 0.706 0.605l3.228 6.558 7.264 1.059c0.504 0.050 0.807 0.303 0.807 0.656z\"></path></svg></svg-icon> </span> </button> <button class=\"btn btn-clear\" ng-click=\"$event.stopPropagation(); $ctrl.EventQuestionsActions.undelete(question)\"> <svg-icon icon=\"retore\" class=\"ui-f-grey-5 ui-f-h-grey-2 question__actions-button ng-isolate-scope\"><svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>retore</title><path d=\"M12 0c-6.628 0-12 5.372-12 12s5.372 12 12 12 12-5.372 12-12-5.372-12-12-12zM12 23.284c-6.234 0-11.284-5.050-11.284-11.284s5.050-11.284 11.284-11.284 11.284 5.050 11.284 11.284-5.050 11.284-11.284 11.284z\"></path><path d=\"M12.525 7.338c-2.575 0-4.659 2.088-4.662 4.662h-1.556l2.072 2.072 2.072-2.072h-1.556c0-2.003 1.622-3.625 3.625-3.625 2 0 3.628 1.625 3.628 3.625 0.003 2-1.616 3.622-3.616 3.625-0.756 0-1.494-0.234-2.109-0.675l-0.734 0.744c2.034 1.569 4.959 1.191 6.528-0.844 0.628-0.816 0.969-1.819 0.969-2.85 0.006-2.575-2.084-4.662-4.659-4.662v0z\"></path></svg></svg-icon> </button> </span> </div>"))

  (test-case "parse-timestamp"
    (check-equal? (parse-timestamp "10 Sep, 7:16pm")
                  "2018-09-10T19:16:00")
    (check-equal? (parameterize ((current-clock (lambda () 1))) (parse-timestamp "today, 7:16pm"))
                  "1969-12-31T19:16:00"))

  (test-case "q->text"
    (check-equal? (q->text q-xexp)
                  "你們會不會講中文"))

  (test-case "q->author"
    (check-equal? (q->author q-xexp)
                  "Anonymous"))

  (test-case "q->timestamp"
    (check-equal? (parameterize ((current-clock (lambda () 1))) (q->timestamp q-xexp))
                  "1969-12-31T19:16:00"))

  (test-case "xexp->q*"
    (define q* (xexp->q* q-xexp))
    (check-equal? (length q*) 1))

  (test-case "xexp->q"
    (define q (xexp->q (car (xexp->q* q-xexp))))
    (check-equal? (slido-question-text q) "你們會不會講中文")
    (check-equal? (slido-question-author q) "Anonymous"))

  (test-case "html->question*"
    (check-equal? (length (html->question* sample-questions.html)) 5)))

(define (add-question* db q*)
  (for/fold ((db db))
            ((q (in-list q*)))
    (add-question db q)))

(define (add-question db q)
  (define q* (slido-db-question* db))
  (if (member q q*)
    db
    (slido-db (slido-db-file db)
              (cons q q*))))

(module+ test
  (test-case "add-question*"
    (define q0 (slido-question 0 0 0))
    (define q1 (slido-question 1 1 1))
    (define q2 (slido-question 2 2 2))
    (check-equal? (slido-db-question* (add-question* (slido-db #f '())
                                                      (list q0 q1 q2)))
                  (list q2 q1 q0)))

  (test-case "add-question"
    (define q0 (slido-question #f #f #f))
    (define q1 (slido-question #t #t #t))
    (check-equal? (slido-db-question* (add-question (slido-db #f (list q0)) q1))
                  (list q1 q0))
    (check-equal? (slido-db-question* (add-question (slido-db #f (list q0)) q0))
                  (list q0))))

(define (slack-channel-id? x)
  (and (string? x)
       (for/or ((chn (in-list the-channel*)))
         (string=? x (channel-slack-id chn)))))

(define (find-channel channel-id)
  (or
    (for/first ((chn (in-list the-channel*))
                #:when (string=? channel-id (channel-slack-id chn)))
      chn)
    (raise-arguments-error 'find-channel "no matching channel" "slack-id" channel-id)))

(define (url->xexp str)
  (call/input-url (string->url str) get-impure-port html->xexp))

(define (find-question* db x)
  (if (slack-channel-id? x)
    (find-question*/channel-id db x)
    (find-question*/time* db x)))

(define (find-question*/time* db lo+hi)
  (define lo-t (add-iso8601-today (car lo+hi)))
  (define hi-t (add-iso8601-today (cdr lo+hi)))
  (question*-within (slido-db-question* db) (cons lo-t hi-t)))

(define (add-iso8601-today str)
  (string-append (date->iso8601 (today)) "T" str ":00"))

(module+ test
  (test-case "add-iso-8601-today"
    (check-equal? (parameterize ((current-clock (lambda () 1))) (add-iso8601-today "11:12"))
                  "1969-12-31T11:12:00")))

(define (find-question*/channel-id db channel-id)
  (define chn (find-channel channel-id))
  (define lo+hi (channel->time-bounds chn))
  (question*-within (slido-db-question* db) lo+hi))

(define (question*-within q* lo+hi)
  (for/list ((q (in-list q*))
             #:when (timestamp-within (slido-question-timestamp q) lo+hi))
    q))

(define (timestamp-within ts lo+hi)
  (define t (iso8601->datetime ts))
  (define lo-t (iso8601->datetime (car lo+hi)))
  (define hi-t (iso8601->datetime (cdr lo+hi)))
  (unless (datetime<=? lo-t hi-t)
    (raise-arguments-error 'timestamp-within "negative-time date range" "lo" (car lo+hi) "hi" (cdr lo+hi) "timestamp" ts))
  (and (datetime<=? lo-t t)
       (datetime<=? t hi-t)))

(module+ test
  (test-case "timestamp-within"
    (check-true (timestamp-within "2018-09-10T19:16:00"
                                  (cons (datetime->iso8601 (datetime 2018 09 10 19 00 00 00))
                                        (datetime->iso8601 (datetime 2018 09 10 20 00 00 00)))))
    (check-true (timestamp-within "2018-09-10T19:16:00"
                                  (cons (datetime->iso8601 (datetime 2018 09 10 19 16 00 00))
                                        (datetime->iso8601 (datetime 2018 09 10 19 16 00 00)))))
    (check-false (timestamp-within "2018-09-10T19:16:00"
                                   (cons (datetime->iso8601 (datetime 2019 09 10 19 00 00 00))
                                         (datetime->iso8601 (datetime 2019 09 10 20 00 00 00)))))
    (check-false (timestamp-within "2018-09-10T19:16:00"
                                   (cons (datetime->iso8601 (datetime 2018 10 10 19 00 00 00))
                                         (datetime->iso8601 (datetime 2018 10 10 20 00 00 00)))))
    (check-false (timestamp-within "2018-09-10T19:16:00"
                                   (cons (datetime->iso8601 (datetime 2018 09 11 19 00 00 00))
                                         (datetime->iso8601 (datetime 2018 09 11 20 00 00 00)))))
    (check-false (timestamp-within "2018-09-10T19:16:00"
                                   (cons (datetime->iso8601 (datetime 2018 09 10 20 00 00 00))
                                         (datetime->iso8601 (datetime 2018 09 10 20 10 00 00)))))))

(define (xexp->time-bounds xxp)
  (define strong* ((sxpath "//strong") xxp))
  (for/or ((s (in-list strong*)))
    (define txt ((sxpath/trim "./text()") s))
    (and (regexp-match? #rx" at$" txt) (string->time-bounds txt))))

(define (string->time-bounds str)
  (define str* (string-split str " - "))
  (unless (= 2 (length str*))
    (raise-argument-error 'string->time-bounds "error parsing string, expected ' - '" str))
  (define lo-str (car str*))
  (define hi-str (string-append (substring lo-str 0 (- (string-length lo-str) 5))
                                (substring (cadr str*) 0 5)))
  (cons (sigplan-string->time lo-str)
        (sigplan-string->time hi-str)))

(define (sigplan-string->time str)
  (datetime->iso8601 (parse-datetime str "EE d LLL y HH:mm")))

(module+ test

  (test-case "sigplan-string->time"
    (check-equal? (sigplan-string->time "Mon 24 Sep 2018 10:30")
                  (datetime->iso8601 (datetime 2018 09 24 10 30 00 00)))
    (check-equal? (sigplan-string->time "Tue 25 Sep 2018 15:52")
                  (datetime->iso8601 (datetime 2018 09 25 15 52 00 00))))

  (test-case "xexp->time-bounds"
    (check-equal? (string->time-bounds "Mon 24 Sep 2018 10:30 - 10:52 at ")
                  (cons (datetime->iso8601 (datetime 2018 09 24 10 30 00))
                        (datetime->iso8601 (datetime 2018 09 24 10 52 00)))))

  (test-case "xexp->time-bounds"
    (check-equal? (xexp->time-bounds (html->xexp "<strong>Mon 24 Sep 2018 10:30 - 10:52 at <a href=\"https://icfp18.sigplan.org/room/icfp-2018-venue2-stifel-theatre\" class=\"room-link navigate\">Stifel Theatre</a></strong>"))
                  (cons (datetime->iso8601 (datetime 2018 09 24 10 30 00))
                        (datetime->iso8601 (datetime 2018 09 24 10 52 00)))))
)

(define (channel->time-bounds chn)
  (if the-timeline*
    (hash-ref the-timeline* (channel-slack-id chn) (lambda () (raise-arguments-error 'channel->time-bounds "channel not in timeline" "channel" chn "timeline" the-timeline*)))
    (xexp->time-bounds (url->xexp (channel-url chn)))))

(module+ test
  (test-case "channel->time-bounds"
    (check-equal? (channel->time-bounds (channel "icfp-incremental-rela" "https://icfp18.sigplan.org/event/icfp-2018-papers-incremental-relational-lenses" '() ""))
                  '("2018-09-25T15:22:00" . "2018-09-25T15:45:00"))))

(define (print-question* q* [channel-id '???])
  ;; TODO use slack API to post questions?
  (unless (null? q*)
    (printf "#~a~n" channel-id)
    (printf "> Questions from sli.do~n"))
  (for ((q (in-list q*)))
    (printf "- ~a~n" (slido-question-text q))))

(define (make-timeline)
  (let ((h (for/hash ((chn (in-list the-channel*)))
             (values (channel-slack-id chn) (channel->time-bounds chn)))))
    (with-output-to-file icfp-timeline.rktd #:exists 'replace
      (lambda () (pretty-write h)))
    (printf "timeline written to '~a'~n" icfp-timeline.rktd)
    (void)))

;; =============================================================================

(module* main racket/base
  (require racket/cmdline racket/match readline readline/pread (submod ".."))
  (define PROMPT #"sli.do> ")
  (define (print-help)
    (printf "Usage:~n")
    (printf "(add <file-name>) : parse questions from file~n")
    (printf "(print <channel-name>) : print questions for the Slack channel~n")
    (printf "(help) : print this message~n")
    (printf "(welcome) : print welcome message~n")
    (printf "(exit) : exit REPL~n")
    (void))
  (define (print-welcome)
    (printf "Welcome to the sli.do organizer. Intended use:~n")
    (printf "  1. visit the ICFP sli.do in your browser~n")
    (printf "  2. copy the html containing sli.do questions to a file~n")
    (printf "  3. run `(add <file.html>)` to import the questions~n")
    (printf "  4. run `(print <channel-id>)` to show all questions asked during the session~n")
    (printf "Or, run `(print <HH:mm> <HH:mm>)` to show all questions asked during the given time range on today's date~n")
    (void))
  (define (->string x)
    (cond
      [(string? x)
       x]
      [(symbol? x)
       (symbol->string x)]
      [else
        (raise-argument-error '->string "cannot handle argument" x)]))
  (command-line
    #:program "slido"
    #:args ()
    (print-welcome)
    (let loop ([the-db (init-db)])
      (match (parameterize ([readline-prompt PROMPT]) (read))
       [(list 'add (? path-string? html))
        (define the-db+
          (if (file-exists? html)
            (with-handlers ((exn:fail:contract? (lambda (e)
                                                  (printf "Error reading file:~n  ~a~n" (exn-message e))
                                                  the-db)))
              (add-from-html the-db html))
            (begin
              (printf "add: file does not exist '~a'~n" html)
              the-db)))
        (loop the-db+)]
       [(list 'print pre-channel-id)
        (define channel-id (->string pre-channel-id))
        (if (slack-channel-id? channel-id)
          (print-question* (find-question* the-db channel-id) channel-id)
          (printf "print: invalid channel id '~a'~n" channel-id))
        (loop the-db)]
       [(list 'print lo hi)
        (define lo+hi (cons (->string lo) (->string hi)))
        (print-question* (find-question* the-db lo+hi) lo+hi)
        (loop the-db)]
       [(cons 'exit _)
        (printf "bye now~n")
        (void)]
       [(cons 'welcom _)
        (print-welcome)
        (loop the-db)]
       [(cons 'help _)
        (print-help)
        (loop the-db)]
       [x
        (printf "Invalid input~n")
        (print-help)
        (loop the-db)]))))
